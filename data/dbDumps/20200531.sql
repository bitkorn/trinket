--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-4)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.country (
                                country_id integer NOT NULL,
                                country_name character varying(100) NOT NULL,
                                country_iso character varying(3) NOT NULL,
                                country_member_eu integer DEFAULT 0 NOT NULL,
                                country_currency_euro integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.country OWNER TO postgres;

--
-- Name: quantityunit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.quantityunit (
                                     quantityunit_uuid uuid NOT NULL,
                                     quantityunit_name character varying(100) DEFAULT ''::character varying NOT NULL,
                                     quantityunit_label character varying(100) NOT NULL,
                                     quantityunit_resolution real DEFAULT 1 NOT NULL,
                                     quantityunit_resolution_group character varying(10) DEFAULT ''::character varying NOT NULL,
                                     quantityunit_order_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.quantityunit OWNER TO postgres;

--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.country (country_id, country_name, country_iso, country_member_eu, country_currency_euro) FROM stdin;
2	Ägypten	EG	0	0
3	Albanien	AL	0	0
4	Algerien	DZ	0	0
5	Andorra	AD	0	0
6	Angola	AO	0	0
7	Anguilla	AI	0	0
8	Antigua und Barbuda	AG	0	0
9	Äquatorialguinea	GQ	0	0
10	Argentinien	AR	0	0
11	Armenien	AM	0	0
12	Aruba	AW	0	0
13	Aserbaidschan	AZ	0	0
14	Äthiopien	ET	0	0
15	Australien	AU	0	0
16	Azoren	PT	0	0
17	Bahamas	BS	0	0
18	Bahrain	BH	0	0
19	Bangladesch	BD	0	0
20	Barbados	BB	0	0
21	Barbuda (Antigua und Barbuda)	AG	0	0
22	Belarus (Weißrussland)	BY	0	0
23	Belgien	BE	1	1
24	Belize	BZ	0	0
25	Benin	BJ	0	0
26	Bermuda	BM	0	0
27	Bhutan	BT	0	0
28	Bolivien	BO	0	0
29	Bonaire	BQ	0	0
30	Bosnien-Herzegowina	BA	0	0
31	Botswana	BW	0	0
32	Brasilien	BR	0	0
33	Brunei	BN	0	0
34	Bulgarien	BG	1	0
35	Burkina Faso	BF	0	0
36	Burundi	BI	0	0
37	Ceuta	ES	0	0
38	Chile	CL	0	0
39	China, Volksrepublik	CN	0	0
40	Cook-Inseln	CK	0	0
41	Costa Rica	CR	0	0
42	Côte d’Ivoire (Elfenbeinküste)	CI	0	0
43	Curaçao	CW	0	0
44	Dänemark	DK	1	0
48	Deutschland	DE	1	1
49	Dominica	DM	0	0
50	Dominikanische Republik	DO	0	0
51	Dschibuti	DJ	0	0
52	Ecuador	EC	0	0
53	El Salvador	SV	0	0
54	Elfenbeinküste (Côte d’Ivoire)	CI	0	0
55	England	GB	1	0
56	Eritrea	ER	0	0
57	Estland	EE	1	1
58	Färöer-Inseln	FO	0	0
59	Fidschi	FJ	0	0
60	Finnland	FI	1	1
61	Frankreich	FR	1	1
65	Französisch-Guyana	GF	0	0
66	Französisch-Polynesien	PF	0	0
67	Gabun	GA	0	0
68	Gambia	GM	0	0
69	Gaza (Westjordanland)	PS	0	0
70	Georgien	GE	0	0
71	Ghana	GH	0	0
72	Gibraltar	GI	0	0
73	Grenada	GD	0	0
74	Griechenland	GR	1	1
75	Grönland	GL	0	0
77	Großbritannien England	GB	1	0
78	Großbritannien Nordirland	GB	1	0
79	Großbritannien Schottland	GB	1	0
80	Großbritannien Wales	GB	1	0
81	Guadeloupe	GP	0	0
82	Guam	GU	0	0
83	Guatemala	GT	0	0
84	Guinea	GN	0	0
85	Guinea-Bissau	GW	0	0
86	Guyana	GY	0	0
88	Haiti	HT	0	0
89	Honduras	HN	0	0
90	Hongkong	HK	0	0
92	Indien	IN	0	0
93	Indonesien	ID	0	0
94	Irak	IQ	0	0
95	Irland	IE	1	1
96	Island	IS	0	0
97	Israel	IL	0	0
99	Italien	IT	1	1
100	Jamaika	JM	0	0
101	Japan	JP	0	0
102	Jemen	YE	0	0
103	Jordanien	JO	0	0
104	Jungferninseln, amerikanische	VI	0	0
105	Jungferninseln, britische	VG	0	0
106	Kaimaninseln	KY	0	0
107	Kambodscha	KH	0	0
108	Kamerun	CM	0	0
109	Kanada	CA	0	0
110	Kanalinseln (Guernsey)	GG	0	0
111	Kanalinseln (Jersey)	JE	0	0
112	Kanarische Inseln	IC	0	0
113	Kap Verde	CV	0	0
114	Kasachstan	KZ	0	0
115	Katar	QA	0	0
116	Kenia	KE	0	0
117	Kirgistan	KG	0	0
118	Kiribati	KI	0	0
119	Kolumbien	CO	0	0
120	Komoren	KM	0	0
121	Kongo (Brazzaville)	CG	0	0
122	Kongo, Demokratische Republik	CD	0	0
123	Korea, Republik (Südkorea)	KR	0	0
124	Kosovo	RS	0	0
125	Kosrae (Föderierte Staaten von Mikoronesien)	FM	0	0
126	Kroatien	HR	1	0
127	Kuwait	KW	0	0
128	Laos	LA	0	0
129	Lesotho	LS	0	0
130	Lettland	LV	1	1
131	Libanon	LB	0	0
132	Liberia	LR	0	0
133	Libyen	LY	0	0
134	Liechtenstein	LI	0	0
135	Litauen	LT	1	1
136	Luxemburg	LU	1	1
137	Macau	MO	0	0
138	Madagaskar	MG	0	0
139	Madeira	PT	0	0
140	Malawi	MW	0	0
141	Malaysia	MY	0	0
142	Malediven	MV	0	0
143	Mali	ML	0	0
144	Malta	MT	1	1
145	Marianen, Nördliche	MP	0	0
146	Marokko	MA	0	0
147	Marshall-Inseln	MH	0	0
148	Martinique	MQ	0	0
149	Mauretanien	MR	0	0
150	Mauritius	MU	0	0
151	Mayotte	YT	0	0
152	Mazedonien	MK	0	0
153	Melilla	ES	0	0
154	Mexiko	MX	0	0
155	Mikronesien (Föderierte Staaten)	FM	0	0
156	Moldawien	MD	0	0
158	Monaco	MC	0	0
159	Mongolei	MN	0	0
160	Montenegro	ME	0	0
161	Montserrat	MS	0	0
162	Mosambik	MZ	0	0
163	Namibia	NA	0	0
164	Nepal	NP	0	0
165	Neukaledonien	NC	0	0
166	Neuseeland	NZ	0	0
167	Nevis (St. Kitts-Nevis)	KN	0	0
168	Nicaragua	NI	0	0
169	Niederlande	NL	1	1
170	Niger	NE	0	0
171	Nigeria	NG	0	0
172	Nordirland	GB	0	0
173	Norwegen	NO	0	0
174	Oman	OM	0	0
180	Österreich	AT	1	1
181	Osttimor	TL	0	0
182	Pakistan	PK	0	0
183	Palau	PW	0	0
184	Panama	PA	0	0
185	Papua-Neuguinea	PG	0	0
186	Paraguay	PY	0	0
187	Peru	PE	0	0
188	Philippinen	PH	0	0
190	Polen	PL	1	0
191	Pohnpei (Föderierte Staaten von Mikronesien)	FM	0	0
192	Portugal (ausgenommen Azoren und Madeira)	PT	1	1
193	Puerto Rico	PR	0	0
194	Réunion	RE	0	0
195	Rota (Nördliche Marianen)	MP	0	0
196	Ruanda	RW	0	0
197	Rumänien	RO	1	0
198	Russland	RU	0	0
199	Saba	BQ	0	0
200	Saipan (Nördliche Marianen)	MP	0	0
201	Salomonen	SB	0	0
202	Sambia	ZM	0	0
203	Samoa	WS	0	0
204	Samoa-Inseln, amerikanische	AS	0	0
205	San Marino	SM	0	0
206	Saudi-Arabien	SA	0	0
207	Schweden	SE	1	0
208	Schweiz	CH	0	0
209	Schottland	GB	0	0
210	Senegal	SN	0	0
211	Serbien	RS	0	0
212	Seychellen	SC	0	0
213	Sierra Leone	SL	0	0
214	Simbabwe	ZW	0	0
215	Singapur	SG	0	0
216	Slowakische Republik	SK	1	1
217	Slowenien	SI	1	1
218	Spanien (ausgenommen Kanarische Inseln: Ceuta und Melilla)	ES	1	1
219	Sri Lanka	LK	0	0
220	St. Barthélemy	BL	0	0
221	St. Christopher (St. Kitts-Nevis)	KN	0	0
222	St. Croix (Amerik. Jungferninseln)	VI	0	0
223	St. Eustatius	BQ	0	0
224	St. John (Amerik. Jungferninseln)	VI	0	0
225	St. Kitts (St. Kitts-Nevis)	KN	0	0
226	St. Lucia	LC	0	0
227	St. Maarten	SX	0	0
228	St. Martin (Guadeloupe)	SX	0	0
229	St. Thomas (Amerik. Jungferninseln)	VI	0	0
230	St. Vincent und Grenadinen	VC	0	0
231	Südafrika	ZA	0	0
232	Surinam	SR	0	0
233	Swasiland	SZ	0	0
234	Tadschikistan	TJ	0	0
235	Tahiti	PF	0	0
236	Taiwan	TW	0	0
237	Tansania	TZ	0	0
238	Thailand	TH	0	0
239	Tinian (Nördliche Marianen)	MP	0	0
240	Tansania	TZ	0	0
241	Thailand	TH	0	0
242	Tinian (Nördliche Marianen)	MP	0	0
243	Togo	TG	0	0
244	Tonga	TO	0	0
245	Tortola (Britische Jungferninseln)	VG	0	0
246	Trinidad und Tobago	TT	0	0
247	Chuuk (Föderierte Staaten von Mikronesien)	FM	0	0
248	Tschad	TD	0	0
249	Tschechische Republik	CZ	1	0
250	Tunesien	TN	0	0
251	Türkei	TR	0	0
252	Turkmenistan	TM	0	0
253	Turks und Caicos-Inseln	TC	0	0
254	Tuvalu	TV	0	0
255	Uganda	UG	0	0
256	Ukraine	UA	0	0
257	Ungarn	HU	1	0
258	Union-Insel (St. Vincent und Grenadinen)	VC	0	0
259	Uruguay	UY	0	0
260	Usbekistan	UZ	0	0
261	Vanuatu	VU	0	0
262	Venezuela	VE	0	0
263	Vereinigte Arabische Emirate	AE	0	0
264	Vereinigte Staaten von Amerika	US	0	0
265	Vietnam	VN	0	0
266	Virgin-Gorda (Brit. Jungferninseln)	VG	0	0
267	Wales	GB	0	0
268	Wallis und Futuna-Inseln	WF	0	0
269	Weißrussland	BY	0	0
270	Westjordanland (Gaza)	PS	0	0
271	Yap (Föderierte Staaten von Mikronesien)	FM	0	0
272	Zentralafrikanische Republik	CF	0	0
273	Zypern	CY	1	1
274	England - Nordirland	GB	1	0
1	Afghanistan	AF	0	0
\.


--
-- Data for Name: quantityunit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.quantityunit (quantityunit_uuid, quantityunit_name, quantityunit_label, quantityunit_resolution, quantityunit_resolution_group, quantityunit_order_priority) FROM stdin;
5c511505-b6df-4244-a52b-7b822017d3a4	Millimeter	mm	0.001	meter	78
4e897116-6301-4361-9ff9-674b23dc31c9	Zentimeter	cm	0.01	meter	80
efede62c-77a1-4c7f-8960-64847d7c63dd	Stunde	Std.	1		90
af84727f-7611-46c4-8b1a-7109e9ce891b	Meter	m	1	meter	85
a8a5a9ca-bb34-4107-ac24-f69cec17f10b	Liter	l	1		75
8ed600f0-93cb-4f5a-b1c7-29790338d705	Quadratmeter	qm	1		83
39964a72-7c38-4046-958e-515c4286c7da	Stück	Stck.	1		100
\.


--
-- Name: country country_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pk PRIMARY KEY (country_id);


--
-- Name: quantityunit quantityunit_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quantityunit
    ADD CONSTRAINT quantityunit_pk PRIMARY KEY (quantityunit_uuid);


--
-- Name: quantityunit_quantityunit_label_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX quantityunit_quantityunit_label_uindex ON public.quantityunit USING btree (quantityunit_label);


--
-- PostgreSQL database dump complete
--
