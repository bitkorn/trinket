## System Requirements
- ZF3 module bitkorn/user

Configure bitkorn/trinket module.config.php:
```php
return [
    'bitkorn_trinket' => [
        'access_rights_octal_folder' => 0775,
        'access_rights_octal_file' => 0664,
        'mailto_address' => 'mail@bitkorn.de', // secure mailto target
        'module_config_routes_services' => [
            /*
             * Use key and value the same. This prevents double entries after applications config merge.
             */
            'BitkornShop\Service\Routes\ConfigRoutes' => 'BitkornShop\Service\Routes\ConfigRoutes'
        ],
    ]
];
```