<?php

namespace Bitkorn\Trinket;

use Bitkorn\Trinket\Controller\Common\ToolsController;
use Bitkorn\Trinket\Controller\Plugin\ValidUuid;
use Bitkorn\Trinket\Controller\Security\EmailController;
use Bitkorn\Trinket\Factory\Controller\Common\ToolsControllerFactory;
use Bitkorn\Trinket\Factory\Controller\Plugin\TranslatorPluginFactory;
use Bitkorn\Trinket\Factory\Filesystem\FolderToolFactory;
use Bitkorn\Trinket\Factory\Filesystem\UploadServiceFactory;
use Bitkorn\Trinket\Factory\Log\LoggerFactory;
use Bitkorn\Trinket\Factory\Service\I18n\NumberFormatServiceFactory;
use Bitkorn\Trinket\Factory\Service\LangServiceFactory;
use Bitkorn\Trinket\Factory\Service\Session\RedisServiceFactory;
use Bitkorn\Trinket\Factory\Service\Template\TemplateRendererServiceFactory;
use Bitkorn\Trinket\Factory\Table\IsoCountryTableFactory;
use Bitkorn\Trinket\Factory\Table\QuantityunitTableFactory;
use Bitkorn\Trinket\Factory\Table\ToolsTableFactory;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Service\Filesystem\UploadService;
use Bitkorn\Trinket\Service\I18n\DateFormatService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\Trinket\Service\Routes\ConfigRoutesService;
use Bitkorn\Trinket\Service\Session\RedisService;
use Bitkorn\Trinket\Service\Template\TemplateRendererService;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Bitkorn\Trinket\Table\QuantityunitTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\Trinket\View\Helper\Content\ShorterText;
use Bitkorn\Trinket\View\Helper\DisplayMessage;
use Bitkorn\Trinket\View\Helper\Form\FormElementErrors;
use Bitkorn\Trinket\View\Helper\Form\SimpleSelect;
use Bitkorn\Trinket\View\Helper\PrintrTextarea;
use Laminas\I18n\Translator\TranslatorServiceFactory;
use Laminas\Log\LoggerAbstractServiceFactory;
use Laminas\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'bitkornlib_security_mailto' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/mailto',
                    'defaults' => [
                        'controller' => EmailController::class,
                        'action' => 'mailto',
                    ],
                ],
            ],
            'bitkorn_trinket_appurls' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/camo-admin-app-urls',
                    'defaults' => [
                        'controller' => ToolsController::class,
                        'action' => 'appUrls'
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            ToolsController::class => ToolsControllerFactory::class,
        ]
    ],
    'controller_plugins' => [
        'factories' => [
            'translate' => TranslatorPluginFactory::class,
        ],
        'invokables' => [
            ValidUuid::class => ValidUuid::class
        ],
        'aliases' => [
            'isValidUuid' => ValidUuid::class
        ]
    ],
    'service_manager' => [
        'abstract_factories' => [
            LoggerAbstractServiceFactory::class,
        ],
        'factories' => [
            'logger' => LoggerFactory::class,
            'translator' => TranslatorServiceFactory::class,
            FolderTool::class => FolderToolFactory::class,
            // service
            NumberFormatService::class => NumberFormatServiceFactory::class,
            DateFormatService::class => DateFormatService::class,
            ConfigRoutesService::class => ConfigRoutesService::class,
            UploadService::class => UploadServiceFactory::class,
            LangService::class => LangServiceFactory::class,
            TemplateRendererService::class => TemplateRendererServiceFactory::class,
            // table
            ToolsTable::class => ToolsTableFactory::class,
            IsoCountryTable::class => IsoCountryTableFactory::class,
            QuantityunitTable::class => QuantityunitTableFactory::class,
            RedisService::class => RedisServiceFactory::class,
        ],
    ],
    'view_helpers' => [
        'aliases' => [
            'form_element_errors' => FormElementErrors::class,
            'formelementerrors' => FormElementErrors::class,
            'formElementErrors' => FormElementErrors::class,
            'FormElementErrors' => FormElementErrors::class,
        ],
        'invokables' => [
            'displayMessage' => DisplayMessage::class,
            'printrTextarea' => PrintrTextarea::class,
            'formelementerrors' => FormElementErrors::class,
            'simpleSelect' => SimpleSelect::class,
            'shorterText' => ShorterText::class
        ],
        'factories' => [
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'layout/nothing' => __DIR__ . '/../view/layout/nothing.phtml',
            'template/content' => __DIR__ . '/../view/template/content.phtml',
            'template/empty' => __DIR__ . '/../view/template/empty.phtml',
        ],
        'template_path_stack' => [
            'Trinket' => __DIR__ . '/../view',
        ],
    ],
    'view_helper_config' => [
        'flashmessenger' => [
            'message_open_format' => '<div%s role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button><ul><li>',
            'message_separator_string' => '</li><li>',
            'message_close_string' => '</li></ul></div>',
        ]
    ],
    'bitkorn_trinket' => [
        'access_rights_octal_folder' => 0775,
        'access_rights_octal_file' => 0664,
        'mailto_address' => 'mail@bitkorn.de', // secure mailto target
        'module_config_routes_services' => [
            /*
             * Use key and value the same. This prevents double entries after applications config merge.
             */
            'BitkornShop\Service\Routes\ConfigRoutes' => 'BitkornShop\Service\Routes\ConfigRoutes'
        ],
        'supported_locales_map' => [
            /**
             * On Linux, to show wich locales are installed, type 'locale -a' in shell.
             */
            'de' => 'de_DE.utf8',
            'en' => 'en_GB.utf8'
        ]
    ]
];
