<?php

namespace Bitkorn\Trinket\Controller;

use Bitkorn\Trinket\Service\LangService;
use Laminas\Http\PhpEnvironment\Response as HttpResponse;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Stdlib\RequestInterface as Request;
use Laminas\Stdlib\ResponseInterface as Response;

class AbstractHtmlController extends AbstractActionController
{

    /**
     *
     * @var Logger
     */
    protected $logger;

    /**
     * @var LangService
     */
    protected $langService;

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param LangService $langService
     */
    public function setLangService(LangService $langService): void
    {
        $this->langService = $langService;
    }

    /**
     * @return HttpResponse|Response
     */
    public function getResponse()
    {
        if($this->response instanceof HttpResponse) {
            return $this->response;
        }
        throw new \RuntimeException('watt!?!? keine HttpResponse in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
    }

    protected function getMatchedRouteName():string
    {
        return $this->getEvent()->getRouteMatch()->getMatchedRouteName();
    }

}
