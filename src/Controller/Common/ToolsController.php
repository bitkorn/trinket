<?php

namespace Bitkorn\Trinket\Controller\Common;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class ToolsController extends AbstractUserController
{
    protected array $config;

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    /**
     * @return JsonModel
     */
    public function appUrlsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $urls = [];
        $urlsContent = '';
        foreach ($this->config['router']['routes'] as $route) {
            $urls[] = $route['options']['route'];
            $urlsContent .= $route['options']['route'] . "\r\n";
        }
        $jsonModel->setArr($urls);
        file_put_contents($this->config['bitkorn_mail']['tmp_folder'] . '/urls', $urlsContent);
        return $jsonModel;
    }
}
