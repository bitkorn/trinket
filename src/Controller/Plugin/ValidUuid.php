<?php

namespace Bitkorn\Trinket\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Laminas\Validator\Uuid;

class ValidUuid extends AbstractPlugin
{
    private Uuid $uuidValidator;

    /**
     * ValidUuid constructor.
     */
    public function __construct()
    {
        $this->uuidValidator = new Uuid();
    }

    /**
     * @param string $guid
     * @return bool
     */
    public function __invoke(string $guid): bool
    {
        return $this->uuidValidator->isValid($guid);
    }
}
