<?php

namespace Bitkorn\Trinket\Controller\Plugin;

use Laminas\I18n\Translator\Translator;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

class TranslatorPlugin extends AbstractPlugin
{
    protected Translator $translator;

    /**
     * @param Translator $translator
     */
    function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function __invoke(string $string, string $textDomain = 'default', $locale = null): string
    {
        return $this->translator->translate($string, $textDomain, $locale);
    }
}
