<?php

namespace Bitkorn\Trinket\Controller\Security;

use Laminas\Mvc\Controller\AbstractActionController;

/**
 * @author allapow
 */
class EmailController extends AbstractActionController
{

    protected string $mailToAddress;

    public function setMailToAddress(string $mailToAddress): void
    {
        $this->mailToAddress = $mailToAddress;
    }

    /**
     */
    public function mailtoAction()
    {
        header("Location: mailto:" . $this->mailToAddress);
        exit();
    }

}
