<?php

namespace Bitkorn\Trinket\Filter;

use Laminas\Filter\FilterChain;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;

class FilterChainStringSanitize extends FilterChain
{

    /**
     * FilterChainStringSanitize constructor.
     * @param null|array|\Traversable $options
     */
    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->attach(new SanitizeStringFilter())->attach(new StripTags())->attach(new StringTrim())->attach(new HtmlEntities());
    }
}
