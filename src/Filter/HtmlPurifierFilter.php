<?php

namespace Bitkorn\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class HtmlPurifierFilter implements FilterInterface
{
    protected \HTMLPurifier $htmlPurifier;

    public function __construct()
    {
        $config = \HTMLPurifier_HTML5Config::createDefault();
        $this->htmlPurifier = new \HTMLPurifier($config);
    }

    public function filter($value)
    {
        return $this->htmlPurifier->purify($value);
    }
}
