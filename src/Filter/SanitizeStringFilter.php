<?php

namespace Bitkorn\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class SanitizeStringFilter implements FilterInterface
{

    public function filter($value)
    {
        return filter_var($value, FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }
}
