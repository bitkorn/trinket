<?php

namespace Bitkorn\Trinket\Service;

use Interop\Container\ContainerInterface;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\ConnectionInterface;
use Laminas\Db\TableGateway\AbstractTableGateway;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Log\Logger;

abstract class AbstractService
{
    protected Logger $logger;
    protected string $message = '';
    protected int $accessRightsFolderOctal = 0775;
    protected DateTimeFormatter $dateFilter;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param \Exception $exception
     * @param string $class Typically __CLASS__
     * @param string $function Typically __FUNCTION__
     */
    protected function log(\Exception $exception, string $class = '', string $function = '')
    {
        $this->logger->err('Service error in ' . $class . '()->' . $function . '()');
        $this->logger->err($exception->getMessage());
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $folder
     * @return bool
     */
    protected function mkdirAndChmod(string $folder): bool
    {
        $success = true;
        if (!file_exists($folder)) {
            if (!mkdir($folder)) {
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while mkdir() ' . $folder);
                $success = false;
            }
            if (!chmod($folder, $this->accessRightsFolderOctal)) {
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while chmod() ' . $folder);
                $success = false;
            }
        }
        return $success;
    }

    /**
     * @param array $fileFormData
     * @param string $uploadFolder
     * @param string $filenameComplete
     * @return bool
     */
    protected function saveFileFromForm(array $fileFormData, string $uploadFolder, string $filenameComplete): bool
    {
        if (!move_uploaded_file($fileFormData['tmp_name'], $uploadFolder . '/' . $filenameComplete)) {
            return false;
        }
        return true;
    }

    /**
     * @param AbstractTableGateway $table
     * @return ConnectionInterface The connection where you can call rollback() and commit().
     */
    protected function beginTransaction(AbstractTableGateway $table): ConnectionInterface
    {
        $adapter = $table->getAdapter();
        if (!$adapter instanceof Adapter) {
            throw new \RuntimeException('$adapter must be of type ' . Adapter::class . '. But ' . get_class($adapter)
                . ' given in ' . __CLASS__ . ' on line ' . __LINE__);
        }
        return $this->beginTransactionAdapter($adapter);
    }

    /**
     * @param Adapter $adapter
     * @return ConnectionInterface
     */
    protected function beginTransactionAdapter(Adapter $adapter): ConnectionInterface
    {
        $conn = $adapter->getDriver()->getConnection();
        return $conn->beginTransaction();
    }

    /**
     * @param string $dateString
     * @param string $dateFormat
     * @return string
     */
    public function formatDateString(string $dateString, string $dateFormat = 'Y-m-d H:i:s.u'): string
    {
        if (empty($dateString)) {
            return '';
        }
        if (!isset($this->dateFilter)) {
            $this->dateFilter = new DateTimeFormatter();
        }
        $this->dateFilter->setFormat($dateFormat);
        $time = $this->dateFilter->filter($dateString);
        if ($time instanceof \DateTime) {
            $time = $time->format('Y-m-d H:i:s.u');
        }
        if (!is_string($time)) {
            return '';
        }
        return $time;
    }

    /**
     * @param \Interop\Container\ContainerInterface $container
     * Overwrite it to avoid repetition in factories.
     */
    public function doFactory(ContainerInterface $container): void
    {
        throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() not implemented yet.');
    }
}
