<?php


namespace Bitkorn\Trinket\Service\Filesystem\Result;


class UploadResult
{
    /**
     * @var string
     */
    private $rootFolder = '';

    /**
     * @var string
     */
    private $folder = '';

    /**
     * @var string
     */
    private $filename = '';

    /**
     * @var string
     */
    private $mimetype = '';

    /**
     * @return string
     */
    public function getRootFolder(): string
    {
        return $this->rootFolder;
    }

    /**
     * @param string $rootFolder
     */
    public function setRootFolder(string $rootFolder): void
    {
        $this->rootFolder = $rootFolder;
    }

    /**
     * @return string
     */
    public function getFolder(): string
    {
        return $this->folder;
    }

    /**
     * @param string $folder
     */
    public function setFolder(string $folder): void
    {
        $this->folder = $folder;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getMimetype(): string
    {
        return $this->mimetype;
    }

    /**
     * @param string $mimetype
     */
    public function setMimetype(string $mimetype): void
    {
        $this->mimetype = $mimetype;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return (!empty($this->rootFolder) && !empty($this->folder) && !empty($this->filename));
    }

    /**
     * @return array ['rootfolder' => '/foo', 'folder' => '/bar', 'filename' => 'baz', 'mimetype' => 'application/vnd.oasis.opendocument.spreadsheet']
     */
    public function toArray(): array
    {
        return ['rootfolder' => $this->rootFolder, 'folder' => $this->folder, 'filename' => $this->filename, 'mimetype' => $this->mimetype];
    }
}
