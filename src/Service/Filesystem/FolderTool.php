<?php

namespace Bitkorn\Trinket\Service\Filesystem;

use Laminas\Log\Logger;

class FolderTool
{
    private Logger $logger;
    private int $accessRightsOctalFolder = 0775;
    private int $accessRightsOctalFile = 0664;

    public function setAccessRightsOctalFolder(int $accessRightsOctalFolder): void
    {
        $this->accessRightsOctalFolder = $accessRightsOctalFolder;
    }

    public function setAccessRightsOctalFile(int $accessRightsOctalFile): void
    {
        $this->accessRightsOctalFile = $accessRightsOctalFile;
    }

    public function purgeDoubleDotPath(string $path, bool $absolute = true): string
    {
        if (empty($segs = explode('/', $path))) {
            return '';
        }
        foreach ($segs as $i => $seg) {
            if(empty($seg)) {
                unset($segs[$i]);
                continue;
            }
            if ($seg == '..' && isset($segs[$i - 1])) {
                unset($segs[$i]);
                unset($segs[$i - 1]);
                return $this->purgeDoubleDotPath(implode('/', $segs));
            }
        }
        return ($absolute ? '/' : '') . implode('/', $segs);
    }

    /**
     * Warning: chmod(): Operation not permitted
     * ...you must be the owner of the file!
     *
     * @param string $filePath
     * @return bool False if the file not exist or chmod() returns false.
     */
    public function chmodFile(string $filePath): bool
    {
        if (!file_exists($filePath)) {
            return false;
        }
        if (!chmod($filePath, $this->accessRightsOctalFile)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ': chmod ' . $this->accessRightsOctalFile . ' ' . $filePath);
        }
        return true;
    }

    /**
     * @param string $folder
     * @return bool
     */
    private function mkdirAndChmod(string $folder): bool
    {
        if (!file_exists($folder)) {
            if (mkdir($folder, $this->accessRightsOctalFolder, true) && chmod($folder, $this->accessRightsOctalFolder)) {
                return true;
            }
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create folder: ' . $folder);
            return false;
        }
        return true;
    }

    /**
     * @param string $path
     */
    public function removeAppendetDirectorySeparators(string &$path)
    {
        if (strrpos($path, DIRECTORY_SEPARATOR) === (strlen($path) - 1)) {
            $path = substr($path, 0, strlen($path) - 1);
            $this->removeAppendetDirectorySeparators($path);
        }
    }

    /**
     * @param string $namespace
     */
    public function removePrependetNamespaceSeparators(string &$namespace)
    {
        if (strpos($namespace, '\\') === 0) {
            $namespace = substr($namespace, 1);
            $this->removePrependetNamespaceSeparators($namespace);
        }
    }

    /**
     * @param string $namespace
     */
    public function removeAppendetNamespaceSeparators(string &$namespace)
    {
        if (strrpos($namespace, '\\') == (strlen($namespace) - 1)) {
            $namespace = substr($namespace, 0, strlen($namespace) - 1);
            $this->removeAppendetNamespaceSeparators($namespace);
        }
    }

    /**
     * @param string $folderAbsolute
     * @return bool
     */
    public function computeFolder(string $folderAbsolute): bool
    {
        return $this->mkdirAndChmod($folderAbsolute);
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $name E.g. module name
     * @param int $unixtime
     * @param string $uuid E.g. userUuid or an accountUuid
     * @return string The folder after the root folder with leading slash.
     */
    public function computeNameDateUuidFolder(string $rootFolderAbsolute, string $name, int $unixtime, string $uuid): string
    {
        $folder = $rootFolderAbsolute;
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . $name;
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . date('Y', $unixtime);
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . date('m', $unixtime);
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . $uuid;
        $this->mkdirAndChmod($folder);
        return substr($folder, strlen($rootFolderAbsolute));
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $name E.g. module name
     * @param int $unixtime
     * @param string $uuid E.g. userUuid or an accountUuid
     * @return string
     */
    public function getNameDateUuidFolderAbsolute(string $rootFolderAbsolute, string $name, int $unixtime, string $uuid): string
    {
        $folder = realpath($rootFolderAbsolute) . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . date('Y', $unixtime) . DIRECTORY_SEPARATOR . date('m', $unixtime) . DIRECTORY_SEPARATOR . $uuid;
        if (!file_exists($folder) || !is_dir($folder)) {
            return '';
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param int $year
     * @param string $month String because it can be e.g. 01
     * @param string $id
     * @return string
     */
    public function computeDateIdFolder(string $rootFolderAbsolute, int $year, string $month, string $id): string
    {
        $folder = realpath($rootFolderAbsolute);
        if ($folder === false) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . ' () Folder ' . $rootFolderAbsolute . ' does not exist.');
        }
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . $year;
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . $month;
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . $id;
        $this->mkdirAndChmod($folder);
        return $folder;
    }

    public function getDateIdFolder(string $rootFolderAbsolute, int $year, string $month, string $id): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            return '';
        }
        $yearFolder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $year;
        if (!file_exists($yearFolder)) {
            return '';
        }
        $monthFolder = $yearFolder . DIRECTORY_SEPARATOR . $month;
        if (!file_exists($monthFolder)) {
            return '';
        }
        $idFolder = $monthFolder . DIRECTORY_SEPARATOR . $id;
        if (!file_exists($idFolder)) {
            return '';
        }
        return $idFolder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $brand
     * @param int $year
     * @param int $month
     * @return string
     */
    public function computeBrandDateFolder(string $rootFolderAbsolute, string $brand, int $year, int $month): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            $this->mkdirAndChmod($rootFolderAbsolute);
        }
        if (!empty($brand)) {
            $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $brand;
        } else {
            $folder = $rootFolderAbsolute;
        }
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        $folder .= DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        $folder .= DIRECTORY_SEPARATOR . str_pad($month, 2, '0', STR_PAD_LEFT);
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $brand
     * @param int $year
     * @param int $month
     * @return string
     */
    public function getBrandDateFolder(string $rootFolderAbsolute, string $brand, int $year, int $month): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            return '';
        }
        if (!empty($brand)) {
            $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $brand;
        } else {
            $folder = $rootFolderAbsolute;
        }
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . str_pad($month, 2, '0', STR_PAD_LEFT);
        if (!file_exists($folder)) {
            return '';
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $brand
     * @param string $year
     * @param string $month
     * @param string $id
     * @return string
     */
    public function computeBrandDateIdFolder(string $rootFolderAbsolute, string $brand, string $year, string $month, string $id): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            $this->mkdirAndChmod($rootFolderAbsolute);
        }
        if (!empty($brand)) {
            $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $brand;
        } else {
            $folder = $rootFolderAbsolute;
        }
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        $folder .= DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        $folder .= DIRECTORY_SEPARATOR . $month;
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        $folder .= DIRECTORY_SEPARATOR . $id;
        if (!file_exists($folder)) {
            $this->mkdirAndChmod($folder);
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $brand
     * @param string $year
     * @param string $month
     * @param string $id
     * @return string
     */
    public function getBrandDateIdFolder(string $rootFolderAbsolute, string $brand, string $year, string $month, string $id): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            return '';
        }
        if (!empty($brand)) {
            $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $brand;
        } else {
            $folder = $rootFolderAbsolute;
        }
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . $month;
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . $id;
        if (!file_exists($folder)) {
            return '';
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param int $id
     * @return string
     */
    public function computeIdFolder(string $rootFolderAbsolute, int $id): string
    {
        $folder = realpath($rootFolderAbsolute);
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . $id;
        $this->mkdirAndChmod($folder);
        return $folder;
    }

    public function deleteDirRecursive(string $dir): bool
    {
        $dirIter = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($dirIter, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            $filePath = $file->getRealPath();
            if ($file->isDir()) {
                $this->deleteDirRecursive($filePath);
            } else {
                unlink($filePath);
            }
        }
        return rmdir($dir);
    }

    /**
     * Remove a dir (all files and folders in it)
     * @param string $path
     */
    public static function rrmdir(string $path)
    {
        $i = new \DirectoryIterator($path);
        foreach ($i as $f) {
            if ($f->isFile()) {
                unlink($f->getRealPath());
            } else if (!$f->isDot() && $f->isDir()) {
                self::rrmdir($f->getRealPath());
                rmdir($f->getRealPath());
            }
        }
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

}
