<?php


namespace Bitkorn\Trinket\Service\Filesystem;


use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\Result\UploadResult;

class UploadService extends AbstractService
{

    /**
     * @param string $filename
     * @return string The mimetype
     */
    public function getMimetype(string $filename): string
    {
        $fileinfo = new \finfo(FILEINFO_MIME);
        $fileinfoFile = $fileinfo->file($filename);
        $fileinfoFileSplittet = explode(';', $fileinfoFile);
        if(empty($fileinfoFileSplittet)) {
            return '';
        }
        return $fileinfoFileSplittet[0];
    }

    /**
     * @param string $folder
     * @param string $filename
     * @param string $salt
     * @return string
     */
    public function computeFilenameNotExist(string $folder, string $filename, string $salt): string
    {
        $now = time();
        $newFilename = $now . '_' . hash('sha1', $now . $salt . rand(1000, 9999)) . '_' . $filename;
        if(file_exists($folder . DIRECTORY_SEPARATOR . $newFilename)) {
            return $this->computeFilenameNotExist($folder, $filename, $salt);
        }
        return $newFilename;
    }

    /**
     * @param array $formDataFile
     * @param string $rootFolder
     * @param string $targetFolder with leading slash
     * @param string $targetFilename
     * @return UploadResult
     */
    public function uploadFormFileToDir(array $formDataFile, string $rootFolder, string $targetFolder, string $targetFilename): UploadResult
    {
        $targetFolderComplete = $rootFolder . $targetFolder;
        $uploadResult = new UploadResult();
        $stringposDot = strrpos($formDataFile['name'], '.');
        if ($stringposDot) {
            $extension = filter_var(substr($formDataFile['name'], ++$stringposDot), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        }
        $filenameComplete = empty($extension) ? $targetFilename : $targetFilename . '.' . $extension;
        $filenameComplete = $this->computeFilenameNotExist($targetFolderComplete, $filenameComplete, 'weissdudochnich');
        if(move_uploaded_file($formDataFile['tmp_name'], $targetFolderComplete . '/' . $filenameComplete)) {
            $uploadResult->setRootFolder($rootFolder);
            $uploadResult->setFolder($targetFolder);
            $uploadResult->setFilename($filenameComplete);
            $uploadResult->setMimetype($this->getMimetype($targetFolderComplete . '/' . $filenameComplete));
        }
        return $uploadResult;
    }
}
