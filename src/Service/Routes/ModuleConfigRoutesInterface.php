<?php

namespace Bitkorn\Trinket\Service\Routes;

/**
 *
 * @author Torsten Brieskorn
 */
interface ModuleConfigRoutesInterface
{

    /**
     * @return array Routes from Module: [0 => ['display' => valueDisplay, 'url' => valueUrl],]
     */
    public function getRoutes();
}
