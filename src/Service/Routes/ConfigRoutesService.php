<?php

namespace Bitkorn\Trinket\Service\Routes;


use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 *
 * @author allapow
 */
class ConfigRoutesService implements FactoryInterface
{
    /**
     *
     * @var \Laminas\Log\Logger
     */
    private $logger;

    /**
     *
     * @var array [0 => ['display' => valueDisplay, 'url' => valueUrl],]
     */
    private $routes = [];

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $this->logger = $container->get('logger');
        $moduleServices = $config['bitkorn_trinket']['module_config_routes_services'];
        foreach ($moduleServices as $moduleService) {
            try {
                /* @var $configRoutesService ModuleConfigRoutesInterface */
                $configRoutesService = $container->get($moduleService);
                $this->routes = array_merge($this->routes, $configRoutesService->getRoutes());
            } catch (\Exception $ex) {
                $this->logger->err($ex->getMessage());
            }
        }
        return $this;
    }

    /**
     *
     * @return array [0 => ['display' => valueDisplay, 'url' => valueUrl],]
     */
    public function getRoutes()
    {
        return $this->routes;
    }
}
