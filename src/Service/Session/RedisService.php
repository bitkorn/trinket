<?php

namespace Bitkorn\Trinket\Service\Session;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class RedisService extends AbstractService
{

    /**
     * @var \Redis
     */
    protected $redis;

    public function __destruct()
    {
        $this->redis->close();
    }

    /**
     * @param \Redis $redis
     */
    public function setRedis(\Redis $redis): void
    {
        $this->redis = $redis;
    }

    /**
     * @return \Redis
     */
    public function getRedis(): \Redis
    {
        return $this->redis;
    }
}
