<?php

namespace Bitkorn\Trinket\Service\I18n;

use Bitkorn\Trinket\Service\LangService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\Log\Logger;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 *
 * @author Torsten Brieskorn
 */
class DateFormatService implements FactoryInterface
{

    /**
     *
     * @var \Laminas\Log\Logger
     */
    private $logger;

    /**
     * Locale to use instead of the default
     *
     * @var string
     */
    protected $locale;

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dateFormat = new DateFormatService();
        $dateFormat->setLogger($container->get('logger'));
        /** @var LangService $langService */
        $langService = $container->get(LangService::class);
        $dateFormat->setLocale($langService->getLocale());
        return $dateFormat;
    }

    /**
     *
     * @param string $targetMode
     * @param string|int $date
     * @param string $format
     * @return boolean
     */
    public function switchDateFormat(string $targetMode, &$date, string $format = 'Y-m-d'): bool
    {
        if ($targetMode == 'string') {
            if (empty($date)) {
                $date = '';
                return true;
            }
            if (is_numeric($date)) {
                $date = date($format, $date);
                return true;
            }
        } elseif ($targetMode == 'integer') {
            if (empty($date)) {
                $date = 0;
                return true;
            }
            try {
                $dateTime = new \DateTime($date);
            } catch (\Exception $exception) {
                return false;
            }
            if ($dateTime) {
                $date = $dateTime->getTimestamp();
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Set locale to use instead of the default.
     *
     * @param string $locale
     * @return DateFormatService
     */
    public function setLocale($locale)
    {
        $this->locale = (string)$locale;
        return $this;
    }

    /**
     * Get the locale to use
     *
     * @return string|null
     */
    public function getLocale()
    {
        if ($this->locale === null) {
            $this->locale = \Locale::getDefault();
        }

        return $this->locale;
    }
}
