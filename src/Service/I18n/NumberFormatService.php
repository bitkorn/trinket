<?php

namespace Bitkorn\Trinket\Service\I18n;

use Bitkorn\Trinket\Service\AbstractService;
use NumberFormatter;
use Locale;

/**
 * Service for PHP class NumberFormatter
 * http://php.net/manual/de/class.numberformatter.php
 *
 * It can be used to display (format) numbers
 * and to parse for persistence in e.g. controllers.
 *
 * @author allapow
 */
class NumberFormatService extends AbstractService
{

    /**
     * number of decimals to use.
     * @var int
     */
    protected int $decimals = 2;

    /**
     * NumberFormat style to use
     * @var int
     */
    protected int $formatStyle;

    /**
     * NumberFormat type to use
     * @var int
     */
    protected int $formatType;

    /**
     * NumberFormatter instances
     * @var array
     */
    protected array $formatters = [];

    /**
     * Text attributes.
     * @var array
     */
    protected array $textAttributes = [];

    /**
     * Locale to use instead of the default
     * @var string
     */
    protected string $locale;

    /**
     * @param float $number
     * @param string|null $formatStyle http://php.net/manual/de/class.numberformatter.php#intl.numberformatter-constants.unumberformatstyle
     * e.g. NumberFormatter::CURRENCY or NumberFormatter::SCIENTIFIC
     * @param null $formatType http://php.net/manual/de/class.numberformatter.php#intl.numberformatter-constants.types
     * e.g. NumberFormatter::TYPE_DOUBLE or NumberFormatter::TYPE_CURRENCY
     * @param null $decimals number of decimals to use.
     * @param array|null $textAttributes
     * @return string
     */
    public function format(float $number, string $formatStyle = null, $formatType = null, $decimals = null, array $textAttributes = null): string
    {
        if (!isset($formatType)) {
            $formatType = $this->getFormatType();
        }
        $formatter = $this->getFormatter($formatStyle, $decimals, $textAttributes);
        return $formatter->format($number, $formatType);
    }

    /**
     * @param string $number
     * @param int|null $formatStyle
     * @param null $formatType
     * @param null $decimals
     * @param array|null $textAttributes
     * @return mixed The value of the parsed number or FALSE on error.
     */
    public function parse(string $number, int $formatStyle = null, $formatType = null, $decimals = null, array $textAttributes = null)
    {
        if (!isset($formatType)) {
            // because: Warning: NumberFormatter::parse(): Unsupported format type 0
            $formatType = NumberFormatter::TYPE_DOUBLE;
        }
        $formatter = $this->getFormatter($formatStyle, $decimals, $textAttributes);
        return $formatter->parse($number, $formatType);
    }

    /**
     * @param int $formatStyle
     * @return NumberFormatService
     * Set format style to use instead of the default
     */
    public function setFormatStyle(int $formatStyle): NumberFormatService
    {
        $this->formatStyle = $formatStyle;
        return $this;
    }

    /**
     * @return int
     * Get the format style to use
     */
    public function getFormatStyle(): int
    {
        if (!isset($this->formatStyle)) {
            $this->formatStyle = NumberFormatter::DECIMAL;
        }
        return $this->formatStyle;
    }

    /**
     * @param int $formatType
     * @return NumberFormatService
     * Set format type to use instead of the default
     */
    public function setFormatType(int $formatType): NumberFormatService
    {
        $this->formatType = $formatType;
        return $this;
    }

    /**
     * @return int
     * Get the format type to use
     */
    public function getFormatType(): int
    {
        if (!isset($this->formatType)) {
            $this->formatType = NumberFormatter::TYPE_DEFAULT;
        }
        return $this->formatType;
    }

    /**
     * @param int $decimals
     * @return NumberFormatService
     * Set number of decimals to use instead of the default.
     */
    public function setDecimals(int $decimals): NumberFormatService
    {
        $this->decimals = $decimals;
        return $this;
    }

    /**
     * @return int
     * Get number of decimals.
     */
    public function getDecimals(): int
    {
        return $this->decimals;
    }

    /**
     * @param string $locale
     * @return NumberFormatService
     * Set locale to use instead of the default.
     */
    public function setLocale(string $locale): NumberFormatService
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string|null
     * Get the locale to use
     */
    public function getLocale(): ?string
    {
        if (!isset($this->locale)) {
            $this->locale = Locale::getDefault();
        }

        return $this->locale;
    }

    /**
     * @return array
     */
    public function getTextAttributes(): array
    {
        return $this->textAttributes;
    }

    /**
     * @param array $textAttributes
     * @return NumberFormatService
     */
    public function setTextAttributes(array $textAttributes): NumberFormatService
    {
        $this->textAttributes = $textAttributes;
        return $this;
    }

    /**
     * @param int|null $formatStyle
     * @param null $decimals
     * @param array|null $textAttributes
     * @return NumberFormatter
     */
    public function getFormatter(int $formatStyle = null, $decimals = null, array $textAttributes = null): NumberFormatter
    {
        $locale = $this->getLocale();
        if (null === $formatStyle) {
            $formatStyle = $this->getFormatStyle();
        }
        if (!is_int($decimals) || $decimals < 0) {
            $decimals = $this->getDecimals();
        }
        if (!is_array($textAttributes)) {
            $textAttributes = $this->getTextAttributes();
        }

        $formatterId = md5(
            $formatStyle . "\0" . $locale . "\0" . $decimals . "\0" . md5(serialize($textAttributes))
        );

        if (isset($this->formatters[$formatterId])) {
            return $this->formatters[$formatterId];
        } else {
            $formatter = new NumberFormatter($locale, $formatStyle);

            if ($decimals !== null) {
                $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, $decimals);
                $formatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, $decimals);
            }

            foreach ($textAttributes as $textAttribute => $value) {
                $formatter->setTextAttribute($textAttribute, $value);
            }

            $this->formatters[$formatterId] = $formatter;
            return $formatter;
        }
    }
}
