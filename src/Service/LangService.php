<?php

namespace Bitkorn\Trinket\Service;

use Laminas\I18n\Translator\Translator;

/**
 * LangService hold shared data.
 *
 * @author allapow
 */
class LangService
{
    public static array $langLocaleMap = [
        'de' => 'de_DE.utf8',
        'en' => 'en_GB.utf8',
    ];

    /**
     * @var string e.g. de
     */
    protected $lang;

    /**
     * @var string e.g. de_DE.utf8
     */
    protected $locale;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @param string $lang e.g. de
     */
    public function setLang(string $lang)
    {
        $this->lang = $lang;
    }

    /**
     * @param string $locale e.g. de_DE.utf8
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;
        $this->translator->setLocale($this->locale);
    }

    /**
     * @param Translator $translator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        if (empty($this->locale)) {
            $this->setLocale(self::$langLocaleMap['de']);
        }
        return $this->locale;
    }
}
