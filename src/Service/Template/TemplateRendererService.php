<?php

namespace Bitkorn\Trinket\Service\Template;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Class TemplateRendererService
 * @package Bitkorn\Trinket\Service\Template
 */
class TemplateRendererService extends AbstractService
{

    /**
     *
     * @var PhpRenderer
     */
    private $renderer;

    /**
     *
     * @var string
     */
    private $template;

    /**
     *
     * @var array
     */
    private $viewVariables = [];

    /**
     * @param PhpRenderer $renderer
     */
    public function setRenderer(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function setViewVariable($key, $value)
    {
        $this->viewVariables[$key] = $value;
    }

    public function setViewVariables($viewVariables)
    {
        $this->viewVariables = array_merge($this->viewVariables, $viewVariables);
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $viewModel = new ViewModel();

        if (!isset($this->template)) {
            return 'empty template in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
        }
        $viewModel->setTemplate($this->template);
        if (!empty($this->viewVariables)) {
            $viewModel->setVariables($this->viewVariables);
        }
        return $this->renderer->render($viewModel);
    }

    /**
     *
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
}
