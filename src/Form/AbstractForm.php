<?php

namespace Bitkorn\Trinket\Form;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Laminas\Form\Form;

/**
 *
 * @author Torsten Brieskorn
 */
class AbstractForm extends Form
{
    protected bool $primaryKeyAvailable = false;
    protected bool $secondaryKeysAvailable = false;

    public function isPrimaryKeyAvailable(): bool
    {
        return $this->primaryKeyAvailable;
    }

    public function setPrimaryKeyAvailable(bool $primaryKeyAvailable): void
    {
        $this->primaryKeyAvailable = $primaryKeyAvailable;
    }

    public function isSecondaryKeysAvailable(): bool
    {
        return $this->secondaryKeysAvailable;
    }

    public function setSecondaryKeysAvailable(bool $secondaryKeysAvailable): void
    {
        $this->secondaryKeysAvailable = $secondaryKeysAvailable;
    }

    public function getMessages(?string $elementName = null): array
    {
        $messages = parent::getMessages($elementName);
        $messagesClear = [];
        foreach ($messages as $elementName => $message) {
            if (empty($message)) {
                continue;
            }
            $m = array_shift($message);
            if (is_array($m)) {
                foreach ($m as $key => $value) {
                    $messagesClear[] = $elementName . '|#|' . $key . ' - ' . $value;
                }
            } elseif (is_string($m)) {
                $messagesClear[] = $elementName . '|#|' . $m;
            } else {
                $messagesClear[] = $elementName . '|#|' . $m;
            }
        }
        return $messagesClear;
    }

    /**
     *
     * @param string $targetMode string | integer
     * @param string $thisElementName The form element name.
     * @param string $thisat
     * @return bool Dateformat default = 'Y-m-d'
     */
    public function switchFormElementDate(string $targetMode, string $thisElementName, string $thisat = 'Y-m-d'): bool
    {
        $date = $this->get($thisElementName)->getValue();
        if ($targetMode == 'string') {
            if (empty($date)) {
                $this->get($thisElementName)->setValue('');
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = '';
                }
                return true;
            }
            if (is_numeric($date)) {
                $this->get($thisElementName)->setValue(date($thisat, $date));
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = date($thisat, $date);
                }
                return true;
            }
        } else if ($targetMode == 'integer') {
            if (empty($date)) {
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = 0;
                }
                return true;
            }
            try {
                $dateTime = new \DateTime($date);
            } catch (\Exception $exception) {
                return false;
            }
            if ($dateTime) {
                $this->get($thisElementName)->setValue($dateTime->getTimestamp());
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = $dateTime->getTimestamp();
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @var array TextElement names for i18n parse & format
     */
    protected $stringNumberElementNames = [];

    public function parseStringNumbers(NumberFormatService $numberFormatService): void
    {
        foreach ($this->stringNumberElementNames as $elementName) {
            if (empty($this->data[$elementName])) {
                $this->data[$elementName] = 0;
            } else {
                $this->data[$elementName] = $numberFormatService->parse($this->data[$elementName]);
            }
        }
    }

    public function formatStringNumbers(NumberFormatService $numberFormatService): void
    {
        foreach ($this->stringNumberElementNames as $elementName) {
            if (empty($this->data[$elementName])) {
                $this->data[$elementName] = 0;
            } else {
                $this->get($elementName)->setValue($numberFormatService->format($this->data[$elementName]));
            }
        }
    }
}
