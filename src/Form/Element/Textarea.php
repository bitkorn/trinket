<?php

namespace Bitkorn\Trinket\Form\Element;

use Bitkorn\Trinket\Form\ElementHtmlEntityDecode;

/**
 *
 * @author Torsten Brieskorn
 */
class Textarea extends ElementHtmlEntityDecode
{
    /**
     * Seed attributes
     *
     * @var array
     */
    protected $attributes = [
        'type' => 'textarea',
    ];
}
