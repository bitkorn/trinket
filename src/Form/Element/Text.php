<?php

namespace Bitkorn\Trinket\Form\Element;

use Bitkorn\Trinket\Form\ElementHtmlEntityDecode;

/**
 *
 * @author Torsten Brieskorn
 */
class Text extends ElementHtmlEntityDecode
{
    /**
     * Seed attributes
     *
     * @var array
     */
    protected $attributes = [
        'type' => 'text',
    ];
}
