<?php


namespace Bitkorn\Trinket\Form\View\Helper;


class FormElementErrors extends \Laminas\Form\View\Helper\FormElementErrors
{
    /**@+
     * @var string Templates for the open/close/separators for message tags
     */
    protected $messageCloseString     = '</li></ul>';
    protected $messageOpenFormat      = '<ul%s><li class="bk-form-element-error">';
    protected $messageSeparatorString = '</li><li class="bk-form-element-error">';

}