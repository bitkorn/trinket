<?php

namespace Bitkorn\Trinket\Form\File;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author Torsten Brieskorn
 */
class CsvFileUploadForm extends AbstractForm implements InputFilterProviderInterface
{

    private $acceptedMimeTypes = 'text/*,text/plain,text/csv,text/comma-separated-values';

    public function init()
    {
        $this->setAttribute('enctype', 'multipart/form-data');

        $file = new \Laminas\Form\Element\File('csv_file');
        $file->setAttributes([
            'class' => 'w3-input w3-border',
//            'accept' => $this->acceptedMimeTypes
        ]);
        $this->add($file);

        $submit = new \Laminas\Form\Element\Submit('submit');
        $submit->setValue('upload');
        $submit->setAttributes([
            'class' => 'w3-button',
        ]);
        $this->add($submit);
    }

    function __construct($name = 'csv_file_form')
    {
        parent::__construct($name);
    }

    public function getInputFilterSpecification()
    {
        return array(
            'csv_file' => array(
                'required' => true,
                'filters' => array(
                ),
                'validators' => array(
                    [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => $this->acceptedMimeTypes,
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'enableHeaderCheck' => true,
                            'messages' => array(
                                \Laminas\Validator\File\MimeType::FALSE_TYPE => "Wrong filetype '%type%', only: $this->acceptedMimeTypes",
                            ),
                        ]
                    ],
                    [
                        'name' => 'filesize',
                        'options' => array(
                            'max' => 1048576,
                        ),
                    ],
                ),
            ),
        );
    }

}

?>
