<?php

namespace Bitkorn\Trinket\Form;

use Laminas\Form\Fieldset;

class AbstractFieldset extends Fieldset
{

    protected $primaryKeyAvailable = true;

    /**
     * @param bool $primaryKeyAvailable
     * @return AbstractFieldset
     */
    public function setPrimaryKeyAvailable(bool $primaryKeyAvailable): AbstractFieldset
    {
        $this->primaryKeyAvailable = $primaryKeyAvailable;
        return $this;
    }

}
