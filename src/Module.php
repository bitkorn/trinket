<?php

namespace Bitkorn\Trinket;

use Bitkorn\Trinket\Controller\Security\EmailController;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\Trinket\Service\Routes\ConfigRoutesService;
use Bitkorn\Trinket\Table\IsoCountryTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Bitkorn\Trinket\View\Helper\LangServiceHelper;
use Bitkorn\Trinket\View\Helper\Route\ConfigRoutesSelect;
use Laminas\EventManager\EventInterface;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\ModuleManager\Feature\ControllerProviderInterface;
use Laminas\ModuleManager\Feature\ServiceProviderInterface;
use Laminas\ModuleManager\Feature\ViewHelperProviderInterface;
use Laminas\ModuleManager\Feature\FormElementProviderInterface;
use Laminas\Mvc\ModuleRouteListener;
use Laminas\Mvc\MvcEvent;
use Laminas\Http\Header\SetCookie;
use Laminas\ServiceManager\ServiceManager;

class Module implements BootstrapListenerInterface, ServiceProviderInterface, ControllerProviderInterface, ViewHelperProviderInterface, FormElementProviderInterface
{

    /**
     * To show wich locales are installes type 'locale -a' in shell.
     *
     * @var array
     */
    private array $langMapping = [
        'de' => 'de_DE.utf8',
        'en' => 'en_GB.utf8'
    ];
    private string $langMappingDefaultKey = 'de';
    private string $locale = '';
    private string $langVar = '';

    /**
     *
     * @param EventInterface $e
     * @return array
     */
    public function onBootstrap(EventInterface $e): array
    {
        if (!$e instanceof MvcEvent) {
            return [];
        }
        $eventManager = $e->getApplication()->getEventManager();
        /*
         * Ohne ModuleRouteListener-attach(EventManager) gibts ein 404 mit message:
         * The requested controller could not be mapped to an existing controller class.
         */
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_ROUTE,
            function (MvcEvent $e) {
                $e->getViewModel()->setVariable('routematch', $e->getRouteMatch()->getMatchedRouteName()); // erreichbar in layouts
                $params = $e->getRouteMatch()->getParams();
                $e->getViewModel()->setVariable('routematchParams', $params);
            });

        $eventManager->attach(MvcEvent::EVENT_ROUTE,
            function (MvcEvent $e) {
                $request = $e->getRequest();
                $response = $e->getResponse();
                if ($request instanceof Request && $response instanceof Response) {
                    $queryParams = $request->getQuery();
                    $langCookieRequest = substr((isset($request->getCookie()->lang) ? $request->getCookie()->lang : ''), 0, 2);
                    $cookieTime = time() + (30 * 24 * 60 * 60);
                    if (isset($queryParams['lang']) && array_key_exists($queryParams['lang'], $this->langMapping)) {
                        $this->langVar = $queryParams['lang'];
                        $this->locale = $this->langMapping[$queryParams['lang']];
                    } else if (empty($langCookieRequest)) {
                        $acceptLangs = explode(',', filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'));
                        $langsWithPrior = [];
                        foreach ($acceptLangs as $acceptLang) {
                            if (strpos($acceptLang, ';')) {
                                $tmpLangsWithPrior = explode(';', $acceptLang);
                                $lang = substr($tmpLangsWithPrior[0], 0, 2);
                                $prior = substr($tmpLangsWithPrior[1], 2);
                            } else {
                                $lang = substr($acceptLang, 0, 2);
                                $prior = 1;
                            }
                            $langsWithPrior[$prior] = $lang;
                        }
                        krsort($langsWithPrior);
                        $this->langVar = substr(current($langsWithPrior), 0, 2);
                        if (empty($this->langVar) || !isset($this->langMapping[$this->langVar])) {
                            $this->langVar = $this->langMappingDefaultKey;
                        }
                        $this->locale = $this->langMapping[$this->langVar];
                    } else {
                        $this->langVar = $langCookieRequest;
                        $this->locale = $this->langMapping[$langCookieRequest];
                    }
                    $langCookie = new SetCookie('lang', $this->langVar, $cookieTime, '/');
                    $response->getHeaders()->addHeader($langCookie);
                    /** @var LangService $langService */
                    $langService = $e->getApplication()->getServiceManager()->get(LangService::class);
                    $langService->setLang($this->langVar);
                    $langService->setLocale($this->locale);
                    $e->getViewModel()->setVariable('lang', $this->langVar); // erreichbar in layouts
                    $e->getViewModel()->setVariable('locale', $this->locale); // erreichbar in layouts
                }
            });

        return [];
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                EmailController::class => function (ServiceManager $serviceManager) {
                    $ctr = new EmailController();
                    $ctr->setMailToAddress($serviceManager->get('config')['bitkorn_trinket']['mailto_address']);
                    return $ctr;
                },
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                /*
                 * Table
                 */
                'Trinket\Table\IsoCountry' => function (ServiceManager $serviceManager) {
                    $table = new IsoCountryTable();
                    $table->setDbAdapter($serviceManager->get('dbDefault'));
                    $table->setLogger($serviceManager->get('logger'));
                    return $table;
                },
                /*
                 * TableX
                 */
                'Trinket\Tablex\Tools' => function (ServiceManager $serviceManager) {
                    $table = new ToolsTable();
                    $table->setDbAdapter($serviceManager->get('dbDefault'));
                    $table->setLogger($serviceManager->get('logger'));
                    return $table;
                },
            ]
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'w3NavbarListItem' => function (ServiceManager $sm) {
                    $helper = new View\Helper\W3\NavbarListItem();
                    $vhm = $sm->get('ViewHelperManager');
                    $helper->setUrlHelper($vhm->get('url'));
                    return $helper;
                },
                'simpleAnchor' => function (ServiceManager $sm) {
                    $helper = new View\Helper\W3\SimpleAnchor();
                    $vhm = $sm->get('ViewHelperManager');
                    $helper->setUrlHelper($vhm->get('url'));
                    return $helper;
                },
                'extendetAnchor' => function (ServiceManager $sm) {
                    $helper = new View\Helper\W3\ExtendetAnchor();
                    $vhm = $sm->get('ViewHelperManager');
                    $helper->setUrlHelper($vhm->get('url'));
                    return $helper;
                },
                'displayContent' => function (ServiceManager $sm) {
                    $helper = new View\Helper\Layout\DisplayContent();
                    $helper->setLogger($sm->get('logger'));
                    return $helper;
                },
                'isoCountryDisplay' => function (ServiceManager $sm) {
                    $helper = new View\Helper\IsoCountryDisplayById();
                    $helper->setIsoCountryTable($sm->get('Trinket\Table\IsoCountry'));
                    return $helper;
                },
                'numberI18n' => function (ServiceManager $sm) {
                    $helper = new View\Helper\I18n\NumberFormat();
                    $helper->setLocale($this->locale);
                    return $helper;
                },
                'configRoutesSelect' => function (ServiceManager $sm) {
                    $helper = new ConfigRoutesSelect();
                    $helper->setConfigRoutesService($sm->get(ConfigRoutesService::class));
                    return $helper;
                },
                'appLang' => function (ServiceManager $sm) {
                    $helper = new LangServiceHelper();
                    $helper->setLogger($sm->get('logger'));
                    $helper->setLangService($sm->get(LangService::class));
                    return $helper;
                },
            ]
        ];
    }

    public function getFormElementConfig()
    {
        return [
            'invokables' => [
                /*
                 * this short names does not working, use instead e.g.
                 * Trinket\Form\Element\Text::class
                 * in 'type' of form element definition.
                 * https://framework.zend.com/manual/2.4/en/modules/zend.form.advanced-use-of-forms.html
                 */
                'Text' => 'Trinket\Form\Element\Text',
                'Textarea' => 'Trinket\Form\Element\Textarea'
            ]
        ];
    }

}
