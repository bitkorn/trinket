<?php

namespace Bitkorn\Trinket\View\Helper\Content;

use Bitkorn\Trinket\Tools\String\StringTool;
use Laminas\View\Helper\AbstractHelper;

class ShorterText extends AbstractHelper
{

    public function __invoke(string $text, int $textLength): string
    {
        return StringTool::shorterText($text, $textLength);
    }
}
