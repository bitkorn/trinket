<?php

namespace Bitkorn\Trinket\View\Helper\Form;

use Laminas\View\Helper\AbstractHelper;

/**
 * @author allapow
 */
class SimpleSelect extends AbstractHelper {

	public function __invoke(array $idAssoc, $currentValue = '', $formFieldName = 'some_name', $cssClass = 'w3-select', $withEmptyValue = true, $emptyValueDisplay = '-- bitte w&auml;hlen --', $emptyValue = '') {
		if (!$idAssoc) {
			return '';
		}
		$select = '<select name="' . $formFieldName . '" id="' . $formFieldName . '" class="' . $cssClass . '">';
		if ($withEmptyValue) {
			$select .= '<option value="' . $emptyValue . '">' . $emptyValueDisplay . '</option>';
		}
		foreach ($idAssoc as $value => $displayValue) {
			$select .= '<option value="' . $value . '" '
					. ($currentValue == $value ? 'selected' : '')
					. '>' . $displayValue . '</option>';
		}
		$select .= '</select>';
		return $select;
	}

}
