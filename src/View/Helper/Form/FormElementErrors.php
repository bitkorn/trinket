<?php

namespace Bitkorn\Trinket\View\Helper\Form;

use Laminas\Form\View\Helper\FormElementErrors as OriginalFormElementErrors;

/**
 * Description of FormElementErrors
 *
 * @author allapow
 */
class FormElementErrors extends OriginalFormElementErrors
{
    protected $attributes = array(
        'class' => 'form-element-error'
    );
}
