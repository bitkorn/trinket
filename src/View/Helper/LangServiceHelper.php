<?php

namespace Bitkorn\Trinket\View\Helper;

use Bitkorn\Trinket\Service\LangService;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class LangServiceHelper extends AbstractViewHelper
{

    /**
     * @var LangService
     */
    protected $langService;

    /**
     * @return object
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param LangService $langService
     */
    public function setLangService(LangService $langService): void
    {
        $this->langService = $langService;
    }

    public function getLang():string
    {
        return $this->langService->getLang();
    }

    public function getLocale():string
    {
        return $this->langService->getLocale();
    }
}
