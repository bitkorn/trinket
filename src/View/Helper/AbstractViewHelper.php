<?php

namespace Bitkorn\Trinket\View\Helper;

use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;

class AbstractViewHelper extends AbstractHelper
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

}
