<?php

namespace Bitkorn\Trinket\View\Helper\W3;

use Laminas\View\Helper\AbstractHelper;

/**
 * In http://www.w3schools.com/ ist ein LI Element alleine in der Navbar
 *
 * @author allapow
 */
class NavbarListItem extends AbstractHelper
{

    /**
     *
     * @var \Laminas\View\Helper\Url
     */
    private $urlHelper;

    public function __invoke($route, $content, $routematch, $cssClass = '')
    {
        $currentNavClass = $route == $routematch ? 'currentnav' : '';
        $currentNavClass .= !empty($cssClass) ? ' ' . $cssClass : '';
        $w3ListItem = "<li class=\"$currentNavClass\">"
                . '<a href="' . $this->urlHelper->__invoke($route) . '">' . $content . '</a>'
                . '</li>';
        return $w3ListItem;
    }

    /**
     *
     * @param \Laminas\View\Helper\Url $urlHelper
     */
    public function setUrlHelper(\Laminas\View\Helper\Url $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }


}
