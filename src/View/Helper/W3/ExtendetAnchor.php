<?php

namespace Bitkorn\Trinket\View\Helper\W3;

use Laminas\View\Helper\AbstractHelper;

/**
 * In http://www.w3schools.com/ ist ein A Element im Dropdown in einem Div
 * <div class="w3-dropdown-content">
 *
 * @author allapow
 */
class ExtendetAnchor extends AbstractHelper
{

    /**
     *
     * @var \Laminas\View\Helper\Url
     */
    private $urlHelper;

    public function __invoke($route, array $routeParams, $content, $routematch, $routematchParams = [], $cssClass = '')
    {
        $currentNavClass = '';
        if ($route == $routematch) {
            if (!empty($routeParams)) {
                $isCurrentNav = true;
                foreach ($routeParams as $routeParamKey => $routeParam) {
                    if ((array_key_exists($routeParamKey, $routematchParams) && $routeParam != $routematchParams[$routeParamKey]) || !array_key_exists($routeParamKey,
                                    $routematchParams)) {
                        $isCurrentNav = false;
                    }
                }
                if ($isCurrentNav) {
                    $currentNavClass = 'currentnav';
                }
            } else {
                $currentNavClass = 'currentnav';
            }
        }
        $currentNavClass .= !empty($cssClass) ? ' ' . $cssClass : '';
        $w3ListItem = "<a class=\"$currentNavClass\""
                . ' href="' . call_user_func($this->urlHelper, $route, $routeParams) . '">' . $content . '</a>';
        return $w3ListItem;
    }

    /**
     *
     * @param \Laminas\View\Helper\Url $urlHelper
     */
    public function setUrlHelper(\Laminas\View\Helper\Url $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

}
