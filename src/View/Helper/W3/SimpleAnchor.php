<?php

namespace Bitkorn\Trinket\View\Helper\W3;

use Laminas\View\Helper\AbstractHelper;

/**
 * In http://www.w3schools.com/ ist ein A Element im Dropdown in einem Div
 * <div class="w3-dropdown-content">
 *
 * @author allapow
 */
class SimpleAnchor extends AbstractHelper
{

    /**
     *
     * @var \Laminas\View\Helper\Url
     */
    private $urlHelper;

    public function __invoke($route, $content, $routematch = null, $cssClass = '')
    {
//        if(!isset($routematch)) {
//            return;
//        }
        $currentNavClass = (($route == $routematch) ? 'currentnav' : '') . ' ' . $cssClass;
//        $currentNavClass .= !empty($cssClass) ? ' ' . $cssClass . ' w3-bar-item w3-mobile': ' w3-bar-item w3-mobile';
        $w3ListItem = "<a class=\"$currentNavClass\""
                . ' href="' . call_user_func($this->urlHelper, $route) . '">' . $content . '</a>';
        return $w3ListItem;
    }

    /**
     *
     * @param \Laminas\View\Helper\Url $urlHelper
     */
    public function setUrlHelper(\Laminas\View\Helper\Url $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }


}
