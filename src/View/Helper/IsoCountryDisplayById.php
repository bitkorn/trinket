<?php

namespace Bitkorn\Trinket\View\Helper;

use Bitkorn\Trinket\Table\IsoCountryTable;
use Laminas\View\Helper\AbstractHelper;

class IsoCountryDisplayById extends AbstractHelper {

    /**
     *
     * @var IsoCountryTable
     */
    private $isoCountryTable;

    public function __invoke($isoCountryId) {
        if(empty($isoCountryId)) {
            return '';
        }
       $isoCountry = $this->isoCountryTable->getIsoCountryById($isoCountryId);
       return $isoCountry['country_name'] . ' (' . $isoCountry['country_iso'] . ')';
    }

    /**
     *
     * @param IsoCountryTable $isoCountryTable
     */
    public function setIsoCountryTable(IsoCountryTable $isoCountryTable)
    {
        $this->isoCountryTable = $isoCountryTable;
    }


}
