<?php

namespace Bitkorn\Trinket\View\Helper\Route;

use Bitkorn\Trinket\Service\Routes\ConfigRoutesService;
use Laminas\View\Helper\AbstractHelper;

/**
 *
 * @author allapow
 */
class ConfigRoutesSelect extends AbstractHelper
{

    /**
     *
     * @var ConfigRoutesService
     */
    private $configRoutesService;

    /**
     *
     * @var array
     */
    private $routes;

    public function __invoke($selectName = 'config_routes', $withEmpty = true)
    {
        if (!isset($this->routes)) {
            $this->routes = $this->configRoutesService->getRoutes();
        }
        $html = '<select class="w3-select" name="' . $selectName . '">';
        if ($withEmpty) {
            $html .= '<option value="0">-- select an internal link --</option>';
        }
        foreach ($this->routes as $route) {
            $html .= '<option value="' . $route['url'] . '">' . $route['display'] . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    /**
     *
     * @param ConfigRoutesService $configRoutesService
     */
    public function setConfigRoutesService(ConfigRoutesService $configRoutesService)
    {
        $this->configRoutesService = $configRoutesService;
    }

}
