<?php

namespace Bitkorn\Trinket\View\Helper;

/**
 * Description of Messages
 *
 * @author allapow
 */
class Messages
{
    public static array $messageSuccessSaveData = ['level' => 'success', 'text' => 'Die Daten wurden erfolgreich gespeichert.'];
    public static array $messageErrorSaveData = ['level' => 'error', 'text' => 'Es gab einen Fehler beim Speichern der Daten.'];
    public static array $messageWarnPrivacyProtec = ['level' => 'warn', 'text' => 'Sie müssen den Datenschutzbestimmungen zustimmen!'];
    public static array $messageWarnEmailExist = ['level' => 'warn', 'text' => 'Die Emailadresse existiert bereits.'];
    public static array $messageErrorUndefined = ['level' => 'error', 'text' => 'Es gab einen unbekannten Fehler.'];
}
