<?php

namespace Bitkorn\Trinket\View\Helper\Layout;

use Laminas\View\Helper\AbstractHelper;

/**
 * @author allapow
 */
class DisplayContent extends AbstractHelper
{

    const CONTENT_TEMPLATE = 'template/content';

    /**
     *
     * @var \Laminas\Log\Logger
     */
    private $logger;

    public function __invoke($sitebarLeft, $content, $sitebarRight, $fullWidth = false)
    {

        $viewModel = new \Laminas\View\Model\ViewModel();
        $viewModel->setTemplate(self::CONTENT_TEMPLATE);
        $contentColumnsL = 12;
        $contentColumnsM = 12;
        $contentColumnsS = 12;
        if (!empty($sitebarLeft)) {
            $contentColumnsL -= 3;
            $contentColumnsM -= 0;
            $contentColumnsS -= 0;
            $viewModel->setVariable('sitebarLeft', $sitebarLeft);
        }
        if (!empty($sitebarRight)) {
            $contentColumnsL -= 3;
            $contentColumnsM -= 0;
            $contentColumnsS -= 0;
            $viewModel->setVariable('sitebarRight', $sitebarRight);
        }
        $viewModel->setVariable('content', $content);
        $viewModel->setVariable('contentColumnsL', $contentColumnsL);
        $viewModel->setVariable('contentColumnsM', $contentColumnsM);
        $viewModel->setVariable('contentColumnsS', $contentColumnsS);
        $viewModel->setVariable('fullWidth', $fullWidth);

        return $this->getView()->render($viewModel);
    }

    public function setLogger(\Laminas\Log\Logger $logger)
    {
        $this->logger = $logger;
    }


}
