<?php

namespace Bitkorn\Trinket\View\Helper;

use Laminas\View\Helper\AbstractHelper;

/**
 * DisplayMessage use Twitter Bootstrap 3 since 2015-02-12
 * ...like the FlashMessenger from ZF2.
 *
 * Snippet from http://zf2.readthedocs.org/en/latest/modules/zend.view.helpers.flash-messenger.html
 * echo $flash->render('error',   array('alert', 'alert-dismissible', 'alert-danger'));
 * echo $flash->render('info',    array('alert', 'alert-dismissible', 'alert-info'));
 * echo $flash->render('default', array('alert', 'alert-dismissible', 'alert-warning'));
 * echo $flash->render('success', array('alert', 'alert-dismissible', 'alert-success'));
 *
 * Bootstrap:
 * <div class="alert alert-success" role="alert">...</div>
 * <div class="alert alert-info" role="alert">...</div>
 * <div class="alert alert-warning" role="alert">...</div>
 * <div class="alert alert-danger" role="alert">...</div>
 *
 * @author allapow
 */
class DisplayMessage extends AbstractHelper {

    public function __invoke(array $message = null) {
        if (isset($message) && is_array($message) && isset($message['level']) && isset($message['text'])) {
            $this->levelCompatibility($message);
            return $this->flashMessageStyle($message['text'], $message['level']);
        }
    }

    private function flashMessageStyle($message, $level) {
        $html = '<div class="w3-panel ' . $level . ' display-message">' . PHP_EOL;
        $html .= "<span onclick=\"this.parentElement.style.display='none'\" class=\"w3-button w3-right\">&times;</span>";
        $html .= '<p>';
        $html .= $message;
        $html .= '</p>';
        $html .= '</div>';
        return $html;
    }

    private function levelCompatibility(&$message) {
        switch ($message['level']) {
            case 'error':
                $message['level'] = 'w3-red';
                break;
            case 'warn':
                $message['level'] = 'w3-orange';
                break;
            case 'success':
                $message['level'] = 'w3-green';
                break;
            case 'info':
                $message['level'] = 'w3-blue';
                break;
        }
    }

}
