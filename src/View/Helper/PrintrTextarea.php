<?php

namespace Bitkorn\Trinket\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class PrintrTextarea extends AbstractHelper
{

    public function __invoke($printrContent = null, string $element = 'textarea')
    {
        if (isset($printrContent) && $printrContent) {
            switch ($element) {
                case 'pre':
                    return '<pre>' . print_r($printrContent, true) . '</pre>';
                case 'textarea':
                    return '<textarea>' . print_r($printrContent, true) . '</textarea>';
            }
        }
    }
}
