<?php

namespace Bitkorn\Trinket\View\Model;

use Bitkorn\Trinket\Json\TrinketJson;
use Laminas\Stdlib\ArrayUtils;
use Laminas\View\Model\JsonModel as LaminasJsonModel;

/**
 * @author Torsten Brieskorn
 */
class JsonModel extends LaminasJsonModel
{
    protected bool $developmentMode = true;

    /**
     * View variables
     * @var array
     */
    protected $variables = ['uuid' => '', 'success' => 0, 'messages' => [], 'error' => '', 'val' => '', 'obj' => '{}', 'arr' => [], 'keyValObjArr' => [], 'count' => 0];

    /**
     * Constructor
     *
     * @param null|array|\Traversable $variables
     * @param array|\Traversable|null $options
     */
    public function __construct($variables = null, $options = null)
    {
        if (null === $variables) {
            $variables = [];
        }
        parent::__construct(array_merge($variables, $this->variables), $options);
    }

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid)
    {
        $this->variables['uuid'] = $uuid;
        return $this;
    }

    public function getVariables()
    {
        $variables = $this->variables;
        if (!$this->developmentMode) {
            unset($variables['desc']);
            unset($variables['messages']);
        }
        return $variables;
    }

    /**
     *
     * @param int $success
     * @return JsonModel
     */
    public function setSuccess(int $success = 0)
    {
        $this->variables['success'] = intval($success);
        return $this;
    }

    /**
     * @param array $data
     * @return JsonModel
     * @deprecated No 'data' key in response, because JS frameworks put his own. Use setVariable() instead.
     */
    public function setData(array $data = [])
    {
        $this->variables['data'] = $data;
        return $this;
    }

    /**
     *
     * @param string $message
     * @return JsonModel
     */
    public function addMessage(string $message)
    {
        if (empty($message)) {
            return $this;
        }
        $this->variables['messages'][] = $message;
        return $this;
    }

    public function addMessages(array $messages = null)
    {
        if (empty($messages) || !is_array($messages)) {
            return $this;
        }
        $this->variables['messages'] = array_merge($this->variables['messages'], $messages);
        return $this;
    }

    public function getMessagesCount(): int
    {
        if (empty($this->variables['messages'])) {
            return 0;
        }
        return count($this->variables['messages']);
    }

    /**
     * @param string $desc
     * @return $this
     * @deprecated because API doc will come from/with OpenAPI
     */
    public function setDesc(string $desc)
    {
        $this->variables['desc'] = $desc;
        return $this;
    }

    public function setVal(string $val)
    {
        $this->variables['val'] = $val;
        return $this;
    }

    public function setObj(array $obj): JsonModel
    {
        $this->variables['obj'] = $obj;
        return $this;
    }

    public function setKeyValObjArr(array $keyValObjArr): JsonModel
    {
        $this->variables['keyValObjArr'] = $keyValObjArr;
        return $this;
    }

    public function setArr(array $arr): JsonModel
    {
        $this->variables['arr'] = $arr;
        return $this;
    }

    public function setCount(int $count): JsonModel
    {
        $this->variables['count'] = $count;
        return $this;
    }

    /**
     * Serialize to JSON with \Bitkorn\Trinket\Json\Json
     *
     * @return string
     */
    public function serialize(): string
    {
        $variables = $this->getVariables();
        if ($variables instanceof \Traversable) {
            $variables = ArrayUtils::iteratorToArray($variables);
        }

        $options = [
            'prettyPrint' => $this->getOption('prettyPrint'),
        ];

        if (null !== $this->jsonpCallback) {
            return $this->jsonpCallback . '(' . TrinketJson::encode($variables, false, $options) . ');';
        }
        return TrinketJson::encode($variables, false, $options);
    }
}
