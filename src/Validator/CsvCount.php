<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

/**
 *
 * @author allapow
 */
class CsvCount extends AbstractValidator
{

    const CSV_COUNT_FALSE = 'false csv count';

    protected $messageTemplates = array(
        self::CSV_COUNT_FALSE => "Anzahl der CSV Felder ('%value%') stimmt nicht überein mit der Vorgabe." // '%value%'
    );

    protected $countTarget;

    public function isValid($value)
    {
        $array = str_getcsv($value);
        if(count($array) == 1 && !isset($array[0])) {
            return true; // ist ja nicht required
        }
        $this->value = count($array);

        if ($this->value != $this->countTarget) {
            $this->error(self::CSV_COUNT_FALSE);
            return FALSE;
        }
        return true;
    }

    public function getCountTarget()
    {
        if ($this->countTarget === null) {
            throw new \RuntimeException('countTarget option is mandatory');
        }
        return $this->countTarget;
    }

    public function setCountTarget($countTarget)
    {
        $this->countTarget = $countTarget;
        return $this;
    }


}

?>
