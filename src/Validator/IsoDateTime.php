<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

/**
 * 1970-01-01 00:00:00
 * @author allapow
 */
class IsoDateTime extends AbstractValidator
{

    const ISO_DATETIME_FALSE = 'Keine ISO Datum-Zeit Zeichenkette';
    const ISO_DATETIME_FALSE_STRING = 'Dei Datum-Zeit ist keine Zeichenkette';

    protected $messageTemplates = [
        self::ISO_DATETIME_FALSE        => "Fehlerhafte ISO Datum-Zeit: '%value%'",
        self::ISO_DATETIME_FALSE_STRING => 'Dei Datum-Zeit ist keine Zeichenkette'
    ];

    public function isValid($value)
    {
        $this->setValue($value);
        if (!is_string($this->value)) {
            $this->error(self::ISO_DATETIME_FALSE_STRING);
            return false;
        }
        $year = intval(substr($this->value, 0, 4));
        $month = intval(substr($this->value, 5, 2));
        $day = intval(substr($this->value, 8, 2));
        $hour = intval(substr($this->value, 11, 2));
        $min = intval(substr($this->value, 14, 2));
        $sec = intval(substr($this->value, 17, 2));
        if (strlen($this->value) != 19 || empty($year) || empty($month) || empty($day)
            || $month > 12 || $day > 31
            || substr($this->value, 4, 1) != '-' || substr($this->value, 7, 1) != '-'
            || substr($this->value, 13, 1) != ':' || substr($this->value, 16, 1) != ':'
            || $hour < 0 || $hour > 24 || $min < 0 || $min > 60 || $sec < 0 || $sec > 60
        ) {
            $this->error(self::ISO_DATETIME_FALSE);
            return false;
        }
        return true;
    }

}
