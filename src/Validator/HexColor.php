<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

class HexColor extends AbstractValidator
{

    const INVALID = 'hexInvalid';
    const NOT_HEX_COLOR = 'notHexColor';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected array $messageTemplates = [
        self::INVALID => "Invalid type given. String expected",
        self::NOT_HEX_COLOR => "The input is none hexadecimal color",
    ];

    /**
     *
     * @param string $value
     * @return bool
     */
    public function isValid($value)
    {
        if (!is_string($value) && !is_int($value)) {
            $this->error(self::INVALID);
            return false;
        }

        $this->setValue($value);
        $hashChar = substr($value, 0, 1);
        $hexPart = substr($value, 1);
        if ($hashChar != '#' || (strlen($hexPart) != 3 && strlen($hexPart) != 6) || !ctype_xdigit($hexPart)) {
            $this->error(self::NOT_HEX_COLOR);
            return false;
        }

        return true;
    }

}
