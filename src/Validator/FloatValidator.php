<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

/**
 *
 * @author allapow
 */
class FloatValidator extends AbstractValidator
{

    const FLOAT_FALSE = 'kein float';

    protected array $messageTemplates = [
        self::FLOAT_FALSE => "Falsches Dezimalzahlen-Format: '%value%'"
    ];

    public function isValid($value)
    {
        $this->setValue($value);
        $valueFloat = floatval($value);
        if ($this->value . '' != $valueFloat . '') {
            $this->error(self::FLOAT_FALSE);
            return FALSE;
        }
        return true;
    }

}
