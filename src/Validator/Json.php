<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

/**
 *
 * @author allapow
 */
class Json extends AbstractValidator
{

    const FLOAT_FALSE = 'kein JSON';

    protected $messageTemplates = array(
        self::FLOAT_FALSE => "Fehlerhafte JSON Syntax" // '%value%'
    );

    public function isValid($value)
    {
        $this->setValue($value);
        $tmp = json_decode($this->value);
        if (NULL === $tmp) {
            $this->error(self::FLOAT_FALSE);
            return FALSE;
        }
        return true;
    }
}

?>
