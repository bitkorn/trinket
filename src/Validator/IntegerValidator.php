<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

/**
 *
 * @author allapow
 */
class IntegerValidator extends AbstractValidator
{

    const INTEGER_FALSE = 'kein int';

    protected array $messageTemplates = [
        self::INTEGER_FALSE => "Falsches Integer-Format: '%value%'"
    ];

    public function isValid($value)
    {
        $this->setValue($value);
        $valueInt = intval($value);
        if ($this->value . '' != $valueInt . '') {
            $this->error(self::INTEGER_FALSE);
            return FALSE;
        }
        return true;
    }

}
