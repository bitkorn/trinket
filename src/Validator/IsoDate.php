<?php

namespace Bitkorn\Trinket\Validator;

use Laminas\Validator\AbstractValidator;

/**
 *
 * @author allapow
 */
class IsoDate extends AbstractValidator
{

    const ISO_DATE_FALSE = 'kein ISO Datum';

    protected $messageTemplates = [
        self::ISO_DATE_FALSE => "Fehlerhaftes ISO Datum: '%value%'"
    ];

    public function isValid($value)
    {
        $this->setValue($value);
        $year = intval(substr($this->value, 0, 4));
        $month = intval(substr($this->value, 5, 2));
        $day = intval(substr($this->value, 8, 2));
        if (strlen($this->value) != 10 || empty($year) || empty($month) || empty($day)
            || $month > 12 || $day > 31
            || substr($this->value, 4, 1) != '-' || substr($this->value, 7, 1) != '-') {
            $this->error(self::ISO_DATE_FALSE);
            return FALSE;
        }
        return true;
    }

}
