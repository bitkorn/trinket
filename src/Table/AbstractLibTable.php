<?php

namespace Bitkorn\Trinket\Table;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\TableGateway\AbstractTableGateway;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Log\Logger;

/**
 *
 * @author allapow
 */
class AbstractLibTable extends AbstractTableGateway implements AdapterAwareInterface
{

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var Logger
     */
    protected $logger;

    public static array $columnsStatic = [];

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet();
        $this->initialize();
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \Exception $exception
     * @param string $class Typically __CLASS__
     * @param string $function Typically __FUNCTION__
     */
    protected function log(\Exception $exception, string $class = '', string $function = '')
    {
        $this->logger->err('Database error in ' . $class . '()->' . $function . '()');
        $this->logger->err($exception->getMessage());
    }

    public static function getColumnsStatic(array $excludes = []): array
    {
        return array_diff(self::$columnsStatic, $excludes);
        //$columns = self::$columnsStatic;
        //foreach ($excludes as $exclude) {
        //    if (($key = array_search($exclude, $columns)) !== false) {
        //        unset($columns[$key]);
        //    }
        //}
        //return $columns;
    }

    public function uuid(): string
    {
        /** @var string $platformName PostgreSQL | MySQL */
        $platformName = $this->adapter->getPlatform()->getName();
        switch ($platformName) {
            case 'MySQL':
                $uuid = $this->adapter->query('SELECT UUID() AS new_uuid');
                break;
            case 'PostgreSQL':
                $uuid = $this->adapter->query('SELECT uuid_generate_v4() AS new_uuid');
                break;
            default:
//                $uuid = $this->computeUuid();
                throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() at line ' . __LINE__ . ': Can not find database adapter platform-name');
        }
        $uuidResult = $uuid->execute()->current();
        return $uuidResult['new_uuid'];
    }

    protected function computeUuid(): string
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @param array $values Reference
     * @param bool $alsoEmptyString
     */
    protected function unsetNullFields(array &$values, bool $alsoEmptyString = false)
    {
        foreach ($values as $key => $val) {
            if ($val === null) {
                unset($values[$key]);
            }
            if ($alsoEmptyString && $val === '') {
                unset($values[$key]);
            }
        }
    }

    /**
     * @return string Timestamp with micro seconds (e.g. for PostgreSQL timestamp)
     */
    protected function getTimestamp(): string
    {
        try {
            $dt = new \DateTime('now');
            return $dt->format('Y-m-d H:i:s.u');
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param AbstractEntity $enity An Entity with not empty primaryKey member. Primary key must be from type UUID.
     * @return string
     */
    public function insertWithEntity(AbstractEntity $enity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $enity->setPrimaryKeyValue($uuid);
        $enity->unsetEmptyValues();
        try {
            $insert->values($enity->getStorage());
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    protected function makeJoinDefault(Select $select): void
    {
        throw new \Exception('Not implemented');
    }

    public function truncateTable(): bool
    {
        $stmt = $this->getAdapter()->getDriver()->createStatement('TRUNCATE public.' . $this->getTable() . ' CASCADE;');
        return $stmt->execute()->valid();
    }
}
