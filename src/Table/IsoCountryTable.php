<?php

namespace Bitkorn\Trinket\Table;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;

/**
 *
 * @author allapow
 */
class IsoCountryTable extends AbstractLibTable
{

    /**
     *
     * @var string
     */
    protected $table = 'country';

    public function countIsoCountry()
    {
        $select = $this->sql->select();
        $select->columns([
            'countIC' => new Expression("COUNT('country_id')") //  as countC nicht mit Postgre kompatible
        ]);
        $results = $this->selectWith($select);
        $resultArr = $results->toArray();
        return $resultArr[0]['countIC'];
    }

    public function getIsoCountryById($id)
    {
        $select = $this->sql->select();
        $select->where([
            'country_id' => $id,
        ]);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if ($result->valid() && $result->count() == 1) {
            return $result->current()->getArrayCopy();
        }
        return [];
    }

    public function getIsoCountryIdAssoc()
    {
        $select = $this->sql->select();
        $select->order(['country_member_eu DESC', 'country_name ASC']);
        try {
            $result = $this->selectWith($select);
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->valid() && $result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $res) {
                $idAssoc[$res['country_id']] = $res['country_iso'] . '-' . $res['country_name'];
            }
            return $idAssoc;
        }
        return [];
    }

    public function getIsoCountryIdAssocRow()
    {
        $select = $this->sql->select();
        $select->order(['country_member_eu DESC', 'country_name ASC']);
        try {
            $result = $this->selectWith($select);
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->valid() && $result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $res) {
                $idAssoc[$res['country_id']] = $res;
            }
            return $idAssoc;
        }
        return [];
    }

    public function getIsoCountryEuropeIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->where(['country_member_eu' => 1]);
            $select->order('country_name ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['country_id']] = $res['country_iso'] . '-' . $res['country_name'];
                }
                return $idAssoc;
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    public function getIsoCountryEuroCurrencyIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->where(['country_currency_euro' => 1]);
            $select->order('country_name ASC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['country_id']] = $res['country_iso'] . '-' . $res['country_name'];
                }
                return $idAssoc;
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
