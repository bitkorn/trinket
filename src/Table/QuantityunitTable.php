<?php

namespace Bitkorn\Trinket\Table;

use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class QuantityunitTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'quantityunit';

    /**
     * @param string $quantityunitUuid
     * @return array
     */
    public function getQuantityunit(string $quantityunitUuid): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->where(['quantityunit_uuid' => $quantityunitUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     * @return array
     */
    public function getQuantityunits(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->order('quantityunit_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     * @return array
     */
    public function getQuantityunitUuidLabelAssoc(): array
    {
        $idAssoc = [];
        $resultArr = $this->getQuantityunits();
        foreach ($resultArr as $row) {
            $idAssoc[$row['quantityunit_uuid']] = $row['quantityunit_label'];
        }
        return $idAssoc;
    }

    /**
     * @return array
     */
    public function getQuantityunitUuidAssoc(): array
    {
        $idAssoc = [];
        $resultArr = $this->getQuantityunits();
        foreach ($resultArr as $row) {
            $idAssoc[$row['quantityunit_uuid']] = $row;
        }
        return $idAssoc;
    }
}
