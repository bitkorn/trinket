<?php

namespace Bitkorn\Trinket\Table;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Log\Logger;
use Laminas\Db\Adapter\ParameterContainer;

class ToolsTable extends AbstractLibTable
{
    /**
     * To prevent the call of AbstractTableGateway()->initialize() in AbstractLibTable()->setDbAdapter()
     * @param Adapter $adapter
     * @return AdapterAwareInterface|void
     */
    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }


    /**
     * MySQL
     * @param $table
     * @param $column
     * @return array
     */
    public function getEnumValues($table, $column)
    {
        $parameter = new ParameterContainer([$this->adapter->getCurrentSchema(), $table, $column]);
        $stmt = $this->adapter->createStatement('SELECT SUBSTRING(COLUMN_TYPE,5) AS resultstring '
            . 'FROM information_schema.COLUMNS '
            . 'WHERE TABLE_SCHEMA=? '
            . 'AND TABLE_NAME=? '
            . 'AND COLUMN_NAME=?', $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            if (!empty($current['resultstring'])) {
                return str_getcsv(substr($current['resultstring'], 1, strlen($current['resultstring']) - 2), ',', "'");
            }
        }
        return [];
    }

    /**
     * @param $enum
     * @return array
     */
    public function getEnumValuesPostgreSQL($enum): array
    {
        $query = "SELECT enum_range(NULL::$enum)";
        $stmt = $this->adapter->createStatement($query);
        /** @var Result $result */
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $current = $result->current();
            $arr = explode(',', str_replace(['{', '}'], '', $current['enum_range']));
            return $arr;
        }
        return ['nix'];
    }
}
