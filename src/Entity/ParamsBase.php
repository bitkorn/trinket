<?php

namespace Bitkorn\Trinket\Entity;

use Laminas\Db\Sql\Select;
use Laminas\Validator\Uuid;

/**
 * Class QueryParamsBase
 * @package Bitkorn\Trinket\Entity\Database
 * This class contains the basic parameters for a search request.
 * The setter methods are secure against bad user input.
 */
class ParamsBase
{
    protected Uuid $uuidValid;

    /**
     * @var array Database fields that can be used for sorting ...please, overwrite this.
     */
    protected array $orderFieldsAvailable = [];

    protected bool $success = true;
    protected array $messages = [];

    /**
     * @var string Which field should be used for sorting
     */
    protected string $orderField = '';

    /**
     * @var string Order direction
     */
    protected string $orderDirec = 'ASC';

    /**
     * @var int 0 = side one, 1 = side two, 2 = side three, ...
     */
    protected int $offset = 0;

    /**
     * @var int SQL LIMIT
     */
    protected int $limit = 0;

    /**
     * @var int Number of result rows (after query the database).
     */
    protected int $count = 0;

    /**
     * @var bool If true then $this->computeSelect($select) do nothing
     */
    protected bool $doCount = false;

    public function __construct()
    {
        $this->uuidValid = new Uuid();
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function addMessage(string $message): void
    {
        if (empty($message)) {
            return;
        }
        $this->messages[] = $message;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function getOrderField(): string
    {
        return $this->orderField;
    }

    public function setOrderField(?string $orderField): void
    {
        if (!$orderField) {
            return;
        }
        if (!in_array($orderField, $this->orderFieldsAvailable)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Order field not in orderFieldsAvailable.');
        }
        $this->orderField = $orderField;
    }

    public function getOrderDirec(): string
    {
        return $this->orderDirec;
    }

    public function setOrderDirec(?string $orderDirec): void
    {
        if (!$orderDirec) {
            return;
        }
        if (!in_array(strtolower($orderDirec), ['asc', 'desc'])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Order direction not valid.');
        }
        $this->orderDirec = $orderDirec;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int ($this->offset + 1) * $this->limit: if $this->>limit == 0, it returns 0, else it returns
     */
    public function getOffsetLimitResult(): int
    {
        return ($this->offset + 1) * $this->limit;
    }

    public function setOffset(?int $offset): void
    {
        if ($offset) {
            $this->offset = $offset;
        }
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): void
    {
        if ($limit) {
            $this->limit = $limit;
        }
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(?int $count): void
    {
        if ($count) {
            $this->count = $count;
        }
    }

    public function setDoCount(bool $doCount): void
    {
        $this->doCount = $doCount;
    }

    public function isDoCount(): bool
    {
        return $this->doCount;
    }

    /**
     * @param string $uuid
     * @return bool
     */
    protected function isNotEmptyAndValidUuid(string $uuid): bool
    {
        return !empty($uuid) && $this->uuidValid->isValid($uuid);
    }

    public function setFromParamsArray(array $qp): void
    {
        $this->setOrderField(!empty($qp['order_field']) ? $qp['order_field'] : '');
        $this->setOrderDirec(!empty($qp['order_direc']) ? $qp['order_direc'] : '');
        $this->setOffset(intval(!empty($qp['offset']) ? $qp['offset'] : 0));
        $this->setLimit(intval(!empty($qp['limit']) ? $qp['limit'] : 0));
    }

    /**
     * @param \Laminas\Db\Sql\Select $select
     * @param string $orderDefault Without ORDER BY, order is undefined and perhaps it loose items during pagination.
     */
    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        if (!$this->doCount) {
            if (!empty($this->orderField) && !empty($this->orderDirec)) {
                $select->order($this->orderField . ' ' . $this->orderDirec);
            } else if (!empty($orderDefault)) {
                $select->order($orderDefault);
            }
            if (!empty($this->limit)) {
                $select->limit($this->limit);
            }
            if (!empty($this->offset)) {
                $select->offset($this->offset);
            }
        }
    }
}
