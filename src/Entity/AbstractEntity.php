<?php

namespace Bitkorn\Trinket\Entity;

/**
 * All Entities extends AbstractEntity
 * Entities manage the relation from Data-Keys in Frontend to Data-Keys in database.
 *
 * @author allapow
 */
class AbstractEntity
{

    /**
     * @var array Initial: key=snake case; value=camel case
     */
    protected array $mapping = [];

    protected $isMappingFlipped = false;

    /**
     * @var array key in snake case. To fetch storage with camel case, use $this->>getPropertyStorage().
     */
    protected array $storage = [];

    /**
     * @var array Data to compare with $this->storage
     */
    public array $storageOutside = [];

    /**
     * @var string Can be set by inheriting classes
     */
    protected string $uuid = '';

    /**
     * @var string Used to set $this->uuid in $this->exchangeArray() or to e.g. unset it in $this->storage.
     */
    protected $primaryKey = '';

    /**
     * @var string
     */
    protected $fieldPrefix = '';

    /**
     * Fields for function $this->unsetDbUnusedFields()
     * @var array
     */
    protected $dbUnusedFields = [];

    protected array $databaseFieldsInsert = [];
    protected array $databaseFieldsUpdate = [];

    /**
     * AbstractEntity constructor.
     * @param string $primaryKey
     */
    public function __construct(string $primaryKey = '')
    {
        if (!empty($primaryKey)) {
            $this->primaryKey = $primaryKey;
        }
    }

    /**
     * Flip if data comes with key in camel case ...e.g. HTTP request
     * @param bool $onlyIfNotFlipped
     */
    public function flipMapping(bool $onlyIfNotFlipped = true)
    {
        if ($this->isMappingFlipped && $onlyIfNotFlipped) {
            return;
        } else if ($this->isMappingFlipped && !$onlyIfNotFlipped) {
            $this->mapping = array_flip($this->mapping);
            $this->isMappingFlipped = false;
        } else if (!$this->isMappingFlipped) {
            $this->mapping = array_flip($this->mapping);
            $this->isMappingFlipped = true;
        }
    }

    /**
     * Flip back if data comes from DB, Form etc. ...with snake case as key
     */
    public function flipMappingBack()
    {
        if ($this->isMappingFlipped) {
            $this->mapping = array_flip($this->mapping);
            $this->isMappingFlipped = false;
        }
    }

    /**
     * Sets the values only if there is a corresponding mapping.
     * Exchange $data to an Array.
     *
     * @param array $data
     * @return bool
     */
    protected function exchangeArray(array $data): bool
    {
        $this->storage = [];
        foreach ($this->mapping as $key => $value) {
            if (isset($data[$key])) {
                if ($this->isMappingFlipped) {
                    $this->storage[$this->fieldPrefix . $value] = $data[$key];
                } else {
                    $this->storage[$this->fieldPrefix . $key] = $data[$key];
                }
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        if (!empty($this->primaryKey) && !empty($this->storage[$this->primaryKey])) {
            $this->uuid = $this->storage[$this->primaryKey];
        }
        return true;
    }

    /**
     * Use this if data come from database or form with the same field names like database.
     * @param array $data
     * @return bool
     */
    public function exchangeArrayFromDatabase(array $data): bool
    {
        $this->flipMappingBack();
        return $this->exchangeArray($data);
    }

    /**
     * Use this if data come from the client side.
     * @param array $data
     * @return bool
     */
    public function exchangeArrayFromRequest(array $data): bool
    {
        $this->flipMapping();
        $result = $this->exchangeArray($data);
        $this->flipMappingBack();
        return $result;
    }

    /**
     * Exchange an array with custom mapping.
     * The key in all arrays ($data, $mapping and $this->mapping) must match for data storage.
     * It uses the flipped or not flipped $this->mapping for storage key.
     *
     * @param array $mapping key=operating_license_*; value=custom_names_*
     * @param array $data key=custom_names_*; value=value
     * @return bool
     */
    public function exchangeArrayWithMapping(array $mapping, array $data): bool
    {
        $this->storage = [];
        foreach ($mapping as $keyMap => $keyData) {
            if (isset($data[$keyData]) && array_key_exists($keyMap, $this->mapping)) {
                $this->storage[$this->fieldPrefix . $this->mapping[$keyMap]] = $data[$keyData];
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        if (!empty($this->primaryKey) && !empty($this->storage[$this->primaryKey])) {
            $this->uuid = $this->storage[$this->primaryKey];
        }
        return true;
    }

    /**
     * @return array
     */
    public function getStorage(): array
    {
        return $this->storage;
    }

    public function getStorageInsert(): array
    {
        $arr = [];
        foreach ($this->databaseFieldsInsert as $field) {
            if (!isset($this->storage[$field])) {
                continue;
            }
            $arr[$field] = $this->storage[$field];
        }
        return $arr;
    }

    public function getStorageUpdate(): array
    {
        $arr = [];
        foreach ($this->databaseFieldsUpdate as $field) {
            $arr[$field] = $this->storage[$field];
        }
        return $arr;
    }

    /**
     * Send to the client in camel case.
     * @return array keys in camel case
     */
    public function getPropertyStorage(): array
    {
        $this->flipMappingBack();
        $storage = [];
        foreach ($this->mapping as $key => $val) {
            $storage[$val] = $this->storage[$key];
        }
        return $storage;
    }

    /**
     * @param string $storageKey snake case
     * @return mixed The value or NULL.
     */
    public function getStorageValue(string $storageKey): mixed
    {
        if (!isset($this->storage[$storageKey])) {
            return null;
        }
        return $this->storage[$storageKey];
    }

    public function backupStorageToOutsite(): void
    {
        $this->storageOutside = $this->storage;
    }

    /**
     * @param string $storageKey snake case
     * @return mixed The value or NULL.
     */
    public function getStorageOutsiteValue(string $storageKey): mixed
    {
        if (!isset($this->storageOutside[$storageKey])) {
            return null;
        }
        return $this->storageOutside[$storageKey];
    }

    /**
     * Exchange $data to an Array.
     * @param array $data
     * @return bool
     */
    public function exchangeOutsiteArray($data): bool
    {
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                $this->storageOutside[$this->mapping[$key]] = $value;
            }
        }
        if (empty($this->storageOutside)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return array Storage keys wich have unequal values.
     * @throws \RuntimeException
     */
    public function compareStorageValues()
    {
        if (!isset($this->storage) || !isset($this->storageOutside)) {
            throw new \RuntimeException('One of the storage is empty. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        if (!is_array($this->storage) || !is_array($this->storageOutside)) {
            throw new \RuntimeException('One of the storage is not an Array. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        $storageKeys = array_values($this->mapping);
        $unequals = [];
        foreach ($storageKeys as $storageKey) {
            if (isset($this->storageOutside[$storageKey]) && $this->storage[$storageKey] != $this->storageOutside[$storageKey]) {
                $unequals[] = $storageKey;
            }
        }
        return $unequals;
    }

    public function getMapping(): array
    {
        return $this->mapping;
    }

    public function isMappingFlipped(): bool
    {
        return $this->isMappingFlipped;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        if (!isset($this->primaryKey)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() PrimaryKey is not set.');
        }
        $this->storage[$this->primaryKey] = $this->uuid = $uuid;
    }

    public function setPrimaryKeyValue($value): void
    {
        $this->storage[$this->primaryKey] = $value;
    }

    public function getPrimaryKeyValue()
    {
        return $this->storage[$this->primaryKey];
    }

    /**
     *
     */
    public function unsetPrimaryKey(): void
    {
        if (!isset($this->storage[$this->primaryKey])) {
            return;
        }
        unset($this->storage[$this->primaryKey]);
    }

    /**
     *
     */
    public function unsetEmptyValues(): void
    {
        if (empty($this->storage)) {
            return;
        }
        foreach ($this->storage as $key => $value) {
            if (empty($this->storage[$key])) {
                unset($this->storage[$key]);
            }
        }
    }

    public function unsetDbUnusedFields(): void
    {
        foreach ($this->dbUnusedFields as $field) {
            if (!is_string($field) || empty($field)) {
                continue;
            }
            unset($this->storage[$field]);
        }
    }

    /**
     * @param string $field
     * @return bool
     */
    public function fieldExistDatabase(string $field): bool
    {
        $this->flipMappingBack();
        return in_array($field, $this->mapping);
    }

    /**
     * @return array
     * @deprecated is getz sowieso snake case in $this->storage keys
     */
    public function generateTableInsertArray(): array
    {
        if (empty($this->storage)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() - do not call me without storage.');
        }
        $this->flipMappingBack();
        $insertArray = [];
        foreach ($this->mapping as $dbField) {
            $insertArray[$dbField] = $this->storage[$dbField] ?? null;
        }
        return $insertArray;
    }

    /**
     * Purging to use the instance like a new object.
     */
    public function purge(): void
    {
        $this->flipMappingBack();
        $this->storage = $this->storageOutside = [];
        $this->uuid = '';
        //$this->primaryKey = '';
    }
}
