<?php

namespace Bitkorn\Trinket\Factory\Service\I18n;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Service\LangService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class NumberFormatServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new NumberFormatService();
        $service->setLogger($container->get('logger'));
        /** @var LangService $langService */
        $langService = $container->get(LangService::class);
        $service->setLocale($langService->getLocale());
        return $service;
    }
}
