<?php

namespace Bitkorn\Trinket\Factory\Service\Session;

use Bitkorn\Trinket\Service\Session\RedisService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RedisServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $redis = new \Redis();
        if (!$redis->connect('127.0.0.1', 6379)) {
            throw new ServiceNotCreatedException(__CLASS__ . '() Can not connect to redis server!');
        }

        $service = new RedisService();
        $service->setRedis($redis);
        $service->setLogger($container->get('logger'));
        return $service;
    }
}
