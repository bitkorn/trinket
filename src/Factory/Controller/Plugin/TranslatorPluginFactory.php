<?php

namespace Bitkorn\Trinket\Factory\Controller\Plugin;

use Bitkorn\Trinket\Controller\Plugin\TranslatorPlugin;
use Interop\Container\ContainerInterface;
use Laminas\I18n\Translator\Translator;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TranslatorPluginFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (!$container->has('translator')) {
            throw new ServiceNotFoundException('Zend I18n Translator not configured');
        }
        /** @var Translator $translator */
        $translator = $container->get('translator');
        return new TranslatorPlugin($translator);
    }
}