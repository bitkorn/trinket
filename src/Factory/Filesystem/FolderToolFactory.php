<?php

namespace Bitkorn\Trinket\Factory\Filesystem;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FolderToolFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FolderTool();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setAccessRightsOctalFolder($config['bitkorn_trinket']['access_rights_octal_folder']);
        $service->setAccessRightsOctalFile($config['bitkorn_trinket']['access_rights_octal_file']);
        return $service;
    }
}
