
1. implement some ObserverInterface

2. put this in service-config

3. attach it to the ObserverManager (most in Module.php onBootstrap(\Laminas\Mvc\MvcEvent $e)):

    $observerManager = \BitkornLib\Observer\ObserverManager::getInstance();

    $application = $e->getApplication();

    $someImplementedObserver = $application->getServiceManager()->get('SomeImplementedObserver');

    $observerManager->attach('someEventName.something', $someImplementedObserver);

4. trigger elsewhere the event (perhaps in a controller):

    $observerManager = \BitkornLib\Observer\ObserverManager::getInstance();

    $observerManager->trigger('someEventName.something', ['some_data_key' => $dataValue]);