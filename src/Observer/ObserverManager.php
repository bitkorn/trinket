<?php

namespace Bitkorn\Trinket\Observer;

use Bitkorn\Trinket\Observer\Observer\ObserverInterface;

/**
 * Bei dem ObserverManager registrieren sich die Observer fuer ein bestimmtes Event,
 * das durch eine Zeichenkette repraesentiert wird.
 *
 * Register a concrete Observer in Module.php's onBootstrap().
 *
 * @author allapow
 */
class ObserverManager
{

    private static ObserverManager $_instance;

    /**
     * @var ObserverInterface[]
     */
    private array $observers = [];

    public function attach(string $event, ObserverInterface $observer)
    {
        if (!isset($this->observers[$event])) {
            $this->observers[$event] = [];
        }
        if (!isset($this->observers[$event][get_class($observer)])) {
            $this->observers[$event][get_class($observer)] = $observer;
        }
    }

    /**
     *
     * @param string $event
     * @param array $data
     * @return boolean
     */
    public function trigger(string $event, array $data): bool
    {
        if (!isset($this->observers[$event]) || empty($this->observers[$event]) || !is_array($this->observers[$event])) {
            return false;
        }
        /**
         * @var string $fqClass
         * @var ObserverInterface $observer
         */
        foreach ($this->observers[$event] as $fqClass => $observer) {
            if (!$observer instanceof $fqClass) {
                continue;
            }
            $observer->triggered($data);
        }
        return true;
    }

    public static function getInstance(): ObserverManager
    {
        if (!self::$_instance) {
            self::$_instance = new ObserverManager();
        }
        return self::$_instance;
    }

    /**
     * clone
     *
     * Kopieren der Instanz von aussen ebenfalls verbieten
     */
    protected function __clone()
    {

    }

    /**
     * constructor
     *
     * externe Instanzierung verbieten
     */
    protected function __construct()
    {

    }

}
