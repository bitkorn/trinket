<?php

namespace Bitkorn\Trinket\Observer\Observer\Concrete;

use Bitkorn\Trinket\Observer\Observer\ObserverInterface;
use Laminas\Log\Logger;

/**
 * Dachte eigentlich, dass jedes Modul seine URLs irgendwo rein wirft wenn eine Sitemap angefragt wird.
 *
 * @author allapow
 */
class SitemapObserver implements ObserverInterface
{

    private Logger $logger;

    public function triggered(array $data)
    {
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

}
