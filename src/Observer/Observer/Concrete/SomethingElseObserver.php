<?php

namespace Bitkorn\Trinket\Observer\Observer\Concrete;

use Bitkorn\Trinket\Observer\Observer\ObserverInterface;
use Laminas\Log\Logger;

class SomethingElseObserver implements ObserverInterface
{

    private Logger $logger;

    public function triggered($data)
    {
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

}
