<?php

namespace Bitkorn\Trinket\Observer\Observer;

interface ObserverInterface
{
    public function triggered(array $data);
}
