<?php

namespace Bitkorn\Trinket\Tools\String;

use Exception;

/**
 * Description of StringTool
 *
 * @author allapow
 */
class StringTool
{

    private static string $pregexAlias = "/[a-z0-9]+/";

    /**
     * $inputString macht mit html_entity_decode() alles zu "normalen" Zeichen.
     * Per Regex werden nur Buchstaben und Zahlen durch gelassen.
     *
     * @param string $inputString
     * @return string NO empty String
     */
    public static function computeAlias(string $inputString): string
    {
        $lowerString = strtolower(html_entity_decode($inputString));
        $matches = [];
        $kp = preg_match_all(self::$pregexAlias, $lowerString, $matches);
        if ($kp) {
            return implode('_', $matches[0]);
        } else {
            return '';
        }
    }

    public static function computeUrlsPlusTargetBlank($htmlString): string
    {
        $htmlString = '<?xml version="1.0" encoding="utf-8"?>' . $htmlString; // ohne dem utf-8 (XML Definition) gibt es Muell
        $doc = new \DOMDocument(); // '1.0', 'UTF-8' ...bringt nichts
        $doc->loadHTML($htmlString);

        $aTags = $doc->getElementsByTagName("a");
        foreach ($aTags as $aTag) {
            $aTag->setAttribute('target', '_blank');
        }
        $body = $doc->getElementsByTagName("body");
        return self::DOMInnerHTML($body->item(0));
    }

    /**
     * http://php.net/manual/en/book.dom.php#89718
     * @param \DOMElement $element
     * @return string
     */
    public static function DOMInnerHTML(\DOMNode $element): string
    {
        $innerHTML = "";
        $children = $element->childNodes;
        foreach ($children as $child) {
            $tmp_dom = new \DOMDocument();
            $tmp_dom->appendChild($tmp_dom->importNode($child, true));
            $innerHTML .= trim($tmp_dom->saveHTML());
        }
        return $innerHTML;
    }

    public static function shorterText(string $text, int $textLength): string
    {
        if (empty($text) || empty($textLength)) {
            return '';
        }
        $shorterText = $text;
        if (strlen($text) > $textLength) {
            $shorterText = substr($text, 0, $textLength);
            $posBlank = strrpos($shorterText, ' ');
            $shorterText = substr($shorterText, 0, $posBlank + 1) . ' ...';
        }
        return $shorterText;
    }

    /**
     * @param string $text
     * @param int $length
     * @param string $replaceString
     * @param string $padString
     * @return string The unique string or an empty string on error.
     * @throws Exception
     */
    public static function computeUniqueString(string $text, int $length, string $replaceString = '_', string $padString = '#'): string
    {
        $text = str_replace(' ', $replaceString, preg_replace('/[^a-z ]/i', '', trim($text)));
        $bytes = random_bytes(20);
        $text = substr(bin2hex($bytes) . $replaceString . $text, 0, $length);
        return str_pad($text, $length, $padString, STR_PAD_RIGHT);
    }

    /**
     * Remove all whitespaces.
     * It makes `str_replace([' ', "\n", "\r", "\t", "\v", "\x00"], '', $chars)`
     * @param string $chars
     * @return string
     */
    public static function trimAll(string $chars): string
    {
        return str_replace([' ', "\n", "\r", "\t", "\v", "\x00"], '', $chars);
    }

    public static function trimAndSanitizeString(string $chars): string
    {
        return filter_var(trim($chars), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public static function trimAllAndSanitizeString(string $chars): string
    {
        return filter_var(self::trimAll($chars), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }
}
