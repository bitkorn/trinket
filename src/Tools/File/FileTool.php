<?php

namespace Bitkorn\Trinket\Tools\File;

class FileTool
{

    /**
     * @param string $path
     * @return string Extension if found after a dot, else 'txt'.
     */
    public static function getExtensionFromFilepath(string $path): string
    {
        if (!empty($pos = strrpos($path, '.'))) {
            return substr($path, 1 + $pos);
        }
        return '';
    }

    public static function getFilenameFromFilepath(string $path): string
    {
        $basename = basename($path);
        $pos = strrpos($basename, '.');
        if ($pos) {
            return substr($basename, 0, ++$pos);
        }
        return $basename;
    }

    /**
     * @param string $path
     * @return string|false The MIME Type or false.
     */
    public static function getMimeTypeFromFileinfo(string $path): bool|string
    {
        $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($fileinfo, $path);
        finfo_close($fileinfo);
        return $mimeType;
    }
}
