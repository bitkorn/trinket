<?php


namespace Bitkorn\Trinket\Tools\Http;


class HttpTool
{
    public static function getBaseUrl(): string
    {
        return strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/'))) . '://' . $_SERVER['SERVER_NAME'];
    }
}
