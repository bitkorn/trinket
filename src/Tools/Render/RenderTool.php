<?php

namespace Bitkorn\Trinket\Tools\Render;

class RenderTool
{
    protected static array $contentDispos = ['attachment', 'inline'];

    /**
     * @param string $content
     * @return string Content as ISO 8859-1
     */
    public static function convertUtf8ContentToISO8859(string $content): string
    {
        $fileUTF8 = tmpfile();
        fwrite($fileUTF8, $content);
        $pathUTF8 = stream_get_meta_data($fileUTF8)['uri'];
        $file8859 = tmpfile();
        $path8859 = stream_get_meta_data($file8859)['uri'];
        $iconvCommand = 'iconv -f utf-8 -t iso-8859-1//TRANSLIT ' . $pathUTF8 . ' > ' . $path8859; // //TRANSLIT oder //IGNORE
        $output = [];
        exec($iconvCommand, $output);
        return file_get_contents($path8859);
    }

    /**
     * @param string $content
     * @param string $outFilename
     * @param string $charset
     * @param string $contentType
     */
    public static function echoAttachment(string $content, string $outFilename, string $charset = 'utf-8', string $contentType = 'text/csv'): void
    {
        ob_clean();
        header('Content-Type: ' . $contentType . '; charset=' . $charset);
        header('Content-Disposition: attachment; filename=' . $outFilename);
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $content;
        exit();
    }

    /**
     * @param string $filePath
     * @param string $outFilename
     * @param string $contentType
     * @param string $contentDispo inline || attachment
     */
    public static function outputAttachment(string $filePath, string $outFilename, string $contentType = 'text/csv', string $contentDispo = 'attachment'): void
    {
        if (empty($contentDispo) || !in_array($contentDispo, RenderTool::$contentDispos)) {
            $contentDispo = 'attachment';
        }
        ob_clean();
        header('Content-Type: ' . $contentType . '; charset=utf-8');
        header('Content-Disposition: ' . $contentDispo . '; filename="' . $outFilename . '"');
        header('Content-length: ' . filesize($filePath));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        header("Pragma: no-cache");
        header("Expires: 0");
        $resource = fopen('php://output', 'w');
        $raw = file_get_contents($filePath);
        fwrite($resource, $raw);
        exit();
    }
}
