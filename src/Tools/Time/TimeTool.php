<?php

namespace Bitkorn\Trinket\Tools\Time;

class TimeTool
{
    /**
     * @param int $minutes
     * @return string Minutes formated as hh:mm
     */
    public static function formatMinutes(int $minutes): string
    {
        $h = floor($minutes / 60);
        $m = $minutes % 60;
        return $h . ':' . $m;
    }

    /**
     * @param string $hoursMinutes A formated string like hh:mm
     * @return int
     */
    public static function parseHoursMinutesToMinutes(string $hoursMinutes): int
    {
        $parts = explode(':', $hoursMinutes);
        if (count($parts) == 2) {
            $minutes = intval($parts[0]) * 60 + intval($parts[1]);
        } else if (count($parts) == 3) {
            $minutes = intval($parts[1]) * 60 + intval($parts[2]);
        } else {
            $minutes = intval($hoursMinutes);
        }
        return $minutes;
    }

    public static function isoDateToGerman(?string $isoDate): string
    {
        if (empty($isoDate) || strlen($isoDate) < 10) {
            return '';
        }
        $parts = explode('-', substr($isoDate, 0, 10));
        if (count($parts) != 3) {
            return '';
        }
        return $parts[2] . '.' . $parts[1] . '.' . $parts[0];
    }
}
