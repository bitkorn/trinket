<?php

namespace Bitkorn\Trinket\Tools\FilterTools;

use Laminas\Validator\Uuid;

class UuidsArrayFilter
{
    protected Uuid $uuid;

    public function __construct()
    {
        $this->uuid = new Uuid();
    }

    protected function validUuid(string $uuid): string
    {
        return $this->uuid->isValid($uuid) ? $uuid : '';
    }

    public function filterUuids(array $uuids, string $arrayKey): array
    {
        if (!is_array($uuids) || !isset($uuids[$arrayKey]) || !is_array($uuids[$arrayKey])) {
            return [];
        }
        $uuidsFiltered = filter_var_array($uuids, [$arrayKey => [
            'filter'  => FILTER_CALLBACK,
            'flags'   => FILTER_REQUIRE_ARRAY,
            'options' => [$this, 'validUuid'],
        ]]);
        return $uuidsFiltered[$arrayKey];
    }
}
