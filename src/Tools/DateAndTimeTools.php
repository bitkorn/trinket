<?php

namespace Bitkorn\Trinket\Tools;

/**
 * Description of DateAndTimeTools
 *
 * @author allapow
 */
class DateAndTimeTools
{

    /**
     *
     * @param string $originDate
     * @param int $monthAmount
     * @return string
     */
    public static function computeMonthAmount($originDate, $monthAmount)
    {
        $originTime = strtotime($originDate);
        $years = date('Y', $originTime);
        $months = date('n', $originTime); // 'm' = mit fuehrenden Nullen; 'n' = ohne
        $days = date('d', $originTime);
        $amountYears = floor(abs($monthAmount) / 12); // ohne abs() und negativem $monthAmount KAKKE
        $amountMonth = $monthAmount % 12;
        $yearstemp = $years + $amountYears;
        $monthsTemp = $months + $amountMonth;
        if ($monthsTemp > 12) {
            $monthsTemp -= 12;
            $yearstemp++;
        } elseif ($monthsTemp <= 0) {
            $monthsTemp += 12;
            $yearstemp--;
        }
        $targetDate = $yearstemp . '-' . str_pad($monthsTemp, 2, '0', STR_PAD_LEFT) . '-' . $days;
        return $targetDate;
    }

    public static function computeToFirstDayInMonth($originDate) {
        $originTime = strtotime($originDate);
        $years = date('Y', $originTime);
        $months = date('m', $originTime); // 'm' = mit fuehrenden Nullen; 'n' = ohne
        return $years . '-' . $months . '-01';
    }

    public static function sqlDatetimeToUnixtime(string $sqlDatetime): int
    {
        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $sqlDatetime);
        if ($dateTime === false) {
            return 0;
        }
        return $dateTime->getTimestamp();
    }
}
