<?php

namespace Bitkorn\Trinket\Tools\Filesystem;

use Laminas\Log\Logger;

class FilenameTool
{

    public static function computeSeoFriendlyFilename(string $text, int $maxOutputLength = 150, Logger $logger = null): string
    {
        $textLower = preg_replace(['/ä/i', '/ü/i', '/ö/i', '/ß/i'], ['ae', 'ue', 'oe', 'ss'], strtolower($text));
        $matches = [];
        $countMatches = preg_match_all('/[0-9a-z]*/', $textLower, $matches);
        if ($countMatches < 1) {
            return '';
        }
        $seoFriendlyFilename = '';
        $count = 0;
        foreach ($matches[0] as $match) {
            if (empty($match)) {
                continue;
            }
            if ($count > 0) {
                $seoFriendlyFilename .= '_';
            }
            $seoFriendlyFilename .= $match;
            $count++;
        }
        $length = strlen($seoFriendlyFilename) >= $maxOutputLength ? $maxOutputLength : strlen($seoFriendlyFilename);
        return substr($seoFriendlyFilename, 0, $length);
    }

    public static function computeSnakeCaseName(string $text, int $maxOutputLength = 150): string
    {
        if($maxOutputLength > 0) {
            $maxOutputLength *= -1;
        }
        return str_replace(' ', '_', preg_replace('/[^a-z ]/i', '', trim(substr($text, $maxOutputLength))));
    }
}
