<?php

namespace Bitkorn\Trinket\Tools\Filesystem;

use Laminas\Log\Logger;

class FolderTool
{
    /**
     *
     * @param string $rootFolderAbsolute
     * @param int $year
     * @param int $month
     * @param string $id
     * @return string
     * @deprecated Use \Bitkorn\Trinket\Service\Filesystem\FolderTool
     */
    public static function computeDateIdFolder(string $rootFolderAbsolute, int $year, int $month, string $id): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            mkdir($rootFolderAbsolute);
            chmod($rootFolderAbsolute, 0775);
        }
        $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0775);
        }
        $folder .= DIRECTORY_SEPARATOR . str_pad($month, 2, '0', STR_PAD_LEFT);
        if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0775);
        }
        $folder .= DIRECTORY_SEPARATOR . $id;
        if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0775);
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param int $year
     * @param int $month
     * @param string $id
     * @return string
     * @deprecated Use \Bitkorn\Trinket\Service\Filesystem\FolderTool
     */
    public static function getDateIdFolder(string $rootFolderAbsolute, int $year, int $month, string $id): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            return '';
        }
        $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . str_pad($month, 2, '0', STR_PAD_LEFT);
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . $id;
        if (!file_exists($folder)) {
            return '';
        }
        return $folder;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $id
     * @return string
     * @deprecated Use \Bitkorn\Trinket\Service\Filesystem\FolderTool
     */
    public static function computeIdFolder(string $rootFolderAbsolute, string $id)
    {
        if (!file_exists($rootFolderAbsolute)) {
            mkdir($rootFolderAbsolute);
            chmod($rootFolderAbsolute, 0775);
        }
        $folder = $rootFolderAbsolute . DIRECTORY_SEPARATOR . $id;
        if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0775);
        }
        return $folder;
    }

    /**
     * http://stackoverflow.com/a/3349792/1307876
     *
     * @param string $dir A maybe not empty folder
     * @return boolean True on success
     * @deprecated Use \Bitkorn\Trinket\Service\Filesystem\FolderTool
     */
    public static function deleteDirRecursive(string $dir): bool
    {
        $dirIter = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($dirIter, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        return rmdir($dir);
    }
}
