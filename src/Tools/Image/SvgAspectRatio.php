<?php


namespace Bitkorn\Trinket\Tools\Image;


class SvgAspectRatio
{

    /**
     * @var \SimpleXMLElement
     */
    protected $xml;

    /**
     * @var float
     */
    protected $width;

    /**
     * @var float
     */
    protected $height;

    /**
     * Aspect ratio width:height.
     *
     * @var array [width,height]
     */
    protected $aspectRatio = [1,1];

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    public function loadSvg(string $svgPath): bool
    {
        $this->xml = simplexml_load_file(realpath($svgPath));
        if ($this->xml === false) {
            return false;
        }
        $atts = $this->xml->attributes();
        $this->width = floatval($atts->width);
        $this->height = floatval($atts->height);
        if (empty($this->width) || empty($this->height)) {
            return false;
        }
        $this->aspectRatio = [1, ($this->height / $this->width)];
        return true;
    }
}