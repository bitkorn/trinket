<?php

namespace Bitkorn\Trinket\Tools\Image;

/**
 * Description of ImageScale
 *
 * @author allapow
 */
class ImageScale
{

    const THUMB_WIDTH = 128;
    const THUMB_HEIGTH = 128;

    private static float $sixteenToNineDecimal = 1.7777777;

    /**
     *
     * @var array Key=multiplicator; value=[width, height]
     */
    private static array $sixteenToNines = [
        1 => [16, 9],
        2 => [32, 18],
        4 => [64, 36], // thump XS
        6 => [96, 54],
        8 => [128, 72], // thump
        10 => [160, 90], // preview
        20 => [320, 180], // preview XL
        30 => [480, 270],
        40 => [640, 360],
        50 => [800, 450],
        60 => [960, 540],
        80 => [1280, 720],
        120 => [1920, 1080]
    ];

    /**
     *
     * @param string $fullImagePath
     * @param string $imageName
     * @param string $imageFormat
     * @param int $width
     * @param int $heigth
     * @param int $widthSmall
     * @param int $heigthSmall
     * @return array
     * @throws \ImagickException
     */
    public static function makeThumb($fullImagePath, $imageName, $imageFormat = 'jpg', $width = 128, $heigth = 128, $widthSmall = 64, $heigthSmall = 64, $widthMedium = 200, $heigthMedium = 200)
    {
        $imagick = new \Imagick($fullImagePath . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        if (($geo['width'] / $width) < ($geo['height'] / $heigth)) {
            $imagick->cropImage($geo['width'], floor($heigth * $geo['width'] / $width), 0, (($geo['height'] - ($heigth * $geo['width'] / $width)) / 2));
        } else {
            $imagick->cropImage(ceil($width * $geo['height'] / $heigth), $geo['height'], (($geo['width'] - ($width * $geo['height'] / $heigth)) / 2), 0);
        }
        $imagick->thumbnailimage($width, $heigth, true);
        $imagick->setimageformat($imageFormat);
        $returnArr = [];
        if (file_put_contents($fullImagePath . '/' . $imageName . '_thumb.' . $imageFormat, $imagick->getimageblob())) {
            $returnArr['thumb'] = $imageName . '_thumb.' . $imageFormat;
        }
        $imagick->thumbnailimage($widthMedium, $heigthMedium, true);
        $imagick->setimageformat($imageFormat);
        if (file_put_contents($fullImagePath . '/' . $imageName . '_thumb_medium.' . $imageFormat, $imagick->getimageblob())) {
            $returnArr['thumb_medium'] = $imageName . '_thumb_medium.' . $imageFormat;
        }
        $imagick->thumbnailimage($widthSmall, $heigthSmall, true);
        $imagick->setimageformat($imageFormat);
        if (file_put_contents($fullImagePath . '/' . $imageName . '_thumb_small.' . $imageFormat, $imagick->getimageblob())) {
            $returnArr['thumb_small'] = $imageName . '_thumb_small.' . $imageFormat;
        }
        $imagick->clear();
        $imagick->destroy();
        return $returnArr;
    }

    /**
     * Scale down to 1920x1080.
     * https://de.wikipedia.org/wiki/Full_HD
     *
     * @param string $fullImagePath
     * @param string $imageName
     * @param string $imageFormat
     * @return string The filename.
     * @throws \ImagickException
     */
    public static function makeFullHd($fullImagePath, $imageName, $imageFormat = 'jpg')
    {
        $imagick = new \Imagick($fullImagePath . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        $widthFullHd = self::$sixteenToNines[120][0];
        $heigthFullHd = self::$sixteenToNines[120][1];
        if (($geo['width'] / $geo['height']) < self::$sixteenToNineDecimal) {
            $newHeight = floor($geo['width'] / 16 * 9);
            $imagick->cropImage($geo['width'], $newHeight, 0, (($geo['height'] - $newHeight) / 2));
        } else {
            $newWidth = floor($geo['height'] / 9 * 16);
            $imagick->cropImage($newWidth, $geo['height'], (($geo['width'] - $newWidth) / 2), 0);
//            $imagick->rotateimage(new \ImagickPixel('#00000000'), 90);
        }
        $geoSixteenToNine = $imagick->getimagegeometry();
        if ($geoSixteenToNine['width'] < $widthFullHd || $geoSixteenToNine['height'] < $heigthFullHd) {
//            return '';
        }
        $imagick->scaleimage($widthFullHd, $heigthFullHd, true);
        $imagick->setimageformat($imageFormat);
        if (file_put_contents($fullImagePath . '/' . $imageName . '_fullhd.' . $imageFormat, $imagick->getimageblob())) {
            $imagick->clear();
            $imagick->destroy();
            return $imageName . '_fullhd.' . $imageFormat;
        }
        $imagick->clear();
        $imagick->destroy();
    }

    /**
     * Scale down to some 16:9 with self::$sixteenToNines.
     * https://de.wikipedia.org/wiki/16:9
     * @param string $fullImagePath
     * @param string $imageName
     * @param string $imageFormat
     * @param array $sixteenToNineMultiplicators IndexKeys from self::$sixteenToNines. If empty then takes it all.
     * @return array Filenames, absolute folder, width and height. Indexed with $sixteenToNineMultiplicator.
     * @throws \ImagickException
     */
    public static function makeSixteenToNines($fullImagePath, $imageName, $imageFormat = 'jpg', array $sixteenToNineMultiplicators = []): array
    {
//        $filenames = [];
        if (empty($sixteenToNineMultiplicators)) {
            $sixteenToNineMultiplicators = array_keys(self::$sixteenToNines);
        }
        $imagick = new \Imagick($fullImagePath . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        // make 16:9
        if (($geo['width'] / $geo['height']) <= self::$sixteenToNineDecimal) {
            $newHeight = floor($geo['width'] / 16 * 9);
            $imagick->cropImage($geo['width'], $newHeight, 0, (($geo['height'] - $newHeight) / 2));
        } else {
            $newWidth = floor($geo['height'] / 9 * 16);
            $imagick->cropImage($newWidth, $geo['height'], (($geo['width'] - $newWidth) / 2), 0);
//            $imagick->rotateimage(new \ImagickPixel('#00000000'), 90);
//            $imagick->setimagepage($imagick->getimagewidth(), $imagick->getimageheight(), 0, 0);
//            $imagick->setimageorientation(\Imagick::ORIENTATION_RIGHTTOP);
//            $geo = $imagick->getimagegeometry();
//            $newHeight = floor($geo['width'] / 16 * 9);
//            $imagick->cropImage($geo['width'], $newHeight, 0, (($geo['height'] - $newHeight) / 2));
        }
        krsort($sixteenToNineMultiplicators);
        $images = [];
        foreach ($sixteenToNineMultiplicators as $sixteenToNineMultiplicator) {
            if (!array_key_exists($sixteenToNineMultiplicator, self::$sixteenToNines)) {
                continue;
            }
            $width = self::$sixteenToNines[$sixteenToNineMultiplicator][0];
            $heigth = self::$sixteenToNines[$sixteenToNineMultiplicator][1];
            $imagick->scaleimage($width, $heigth, true);
            $imagick->setimageformat($imageFormat);
            $imageFilename = $imageName . '_' . $sixteenToNineMultiplicator . '.' . $imageFormat;
            file_put_contents($fullImagePath . '/' . $imageFilename, $imagick->getimageblob());
//            $filenames[$sixteenToNineMultiplicator] = $imageFilename;
            $images[$sixteenToNineMultiplicator] = new ImageEntity($fullImagePath, $imageFilename, $width, $heigth);
        }
        $imagick->clear();
        $imagick->destroy();
        return $images;
    }

}
