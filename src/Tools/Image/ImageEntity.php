<?php

namespace Bitkorn\Trinket\Tools\Image;

/**
 * Description of ImageEntity
 *
 * @author allapow
 */
class ImageEntity
{

    public $folderAbsolute;
    public $filename;
    public $width;
    public $height;

    public function __construct($folderAbsolute, $filename, $width, $height)
    {
        $this->folderAbsolute = $folderAbsolute;
        $this->filename = $filename;
        $this->width = $width;
        $this->height = $height;
    }

}
