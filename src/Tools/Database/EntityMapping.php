<?php

namespace Bitkorn\Trinket\Tools\Database;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Filter\Word\UnderscoreToCamelCase;

/**
 *
 * @author Torsten Brieskorn
 */
class EntityMapping
{

    public static function computeEntityMapping(array $tableRow, bool $toCamesCase = false): string
    {
        $mapping = [];
        if($toCamesCase) {
            $filter = new UnderscoreToCamelCase();
        }
        foreach ($tableRow as $key => $value) {
            if($toCamesCase) {
                $mapping[$key] = $filter->filter($key);
            } else {
                $mapping[$key] = $key;
            }
        }
        $mappingString = var_export($mapping, true);
        if (!$mappingString) {
            return '';
        }
        return $mappingString;
    }

    /**
     * @param Adapter $adapter
     * @param string $tableName
     * @return string
     */
    public static function computeEntityMappingAdvanced(Adapter $adapter, string $tableName): string
    {
        $stmt = $adapter->createStatement('SELECT * FROM ' . $tableName . ' LIMIT 1');
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $row = $result->current();
            return EntityMapping::computeEntityMapping($row);
        }
        return 'nothing from ' . __FUNCTION__ . '()';
    }

    /**
     * @param Adapter $adapter
     * @param string $tableName
     * @return string
     */
    public static function computeEntityMappingPostgresql(Adapter $adapter, string $tableName): string
    {
        $stmt = $adapter->createStatement("SELECT column_name FROM information_schema.columns WHERE table_schema='public' AND table_name='$tableName' ORDER BY ordinal_position ASC");
        /** @var Result $result */
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $columns = [];
            do {
                $columnName = $result->current()['column_name'];
                $columns[$columnName] = $columnName;
                $result->next();
            } while ($result->valid());
            return EntityMapping::computeEntityMapping($columns);
        }
        return 'nothing from ' . __FUNCTION__ . '()';
    }
}
