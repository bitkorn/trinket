<?php

namespace Bitkorn\Trinket\Hydrator;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Laminas\Hydrator\HydratorInterface;

class EntityHydrator implements HydratorInterface
{

    /**
     * @inheritDoc
     */
    public function extract(object $object): array
    {
        if (!$object instanceof AbstractEntity) {
            throw new \RuntimeException(__CLASS__ . '() Can not hydrate class of type ' . get_class($object));
        }
        return $object->getStorage();
    }

    /**
     * @inheritDoc
     */
    public function hydrate(array $data, object $object)
    {
        if (!$object instanceof AbstractEntity) {
            throw new \RuntimeException(__CLASS__ . '() Can not hydrate class of type ' . get_class($object));
        }
        $object->exchangeArrayFromDatabase($data);
    }
}
